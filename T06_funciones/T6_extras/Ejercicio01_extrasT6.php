<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Extras Tema 6: funciones - Ejercicio 1</title>
    <link rel="stylesheet" href="css/style.css">
    <!-- Crear una página web para generar de manera aleatoria una combinación de apuesta en la lotería primitiva. 
    Se pedirán 6 números (entre 1 y 49) y el número de serie (entre 1 y 999). 
    El usuario podrá rellenar los números pedidos que desee, dejando en blanco el resto, de modo que la combinación 
    generada respete los números elegidos y genere aleatoriamente el resto hasta completar la combinación (el número de serie también es opcional). 
    El usuario también podrá rellenar de manera opcional una caja de texto como título para su combinación. 
    Usar una función combinacion() que sea llamada para generar la combinación en función de los parámetros recibidos y devuelva el array generado.
    Usar una función imprimeApuesta()que reciba un array y un texto, e imprima en una tabla html con el formato que quieras, el array generado con el 
    texto de cabecera, para mostrar el resultado de la combinacióngenerada. 
    Si la función no recibe ningún texto como cabecera imprimirá "Combinación generada para la Primitiva". -->
</head>

<body>
    <div id="header">
        <h1>Ejercicios Adicionales Tema 6: Funciones</h1>
        <h2>Ejercicio 1</h2>
    </div>

    <div id="content">

        <?php
        // escribimos el formulario:
        echo "<h4>LOTERÍA PRIMITIVA</h4>";
        echo "<label for='apuesta'>Introduce tu apuesta:</label>";
        echo "<form action='Ejercicio01_extrasT6_result.php' name='apuesta' id='loteria' method='get'>";
        for ($i = 1; $i < 8; $i++) {
            if ($i == 7) {
                echo "<br>Serie:&nbsp;<input type='number' name='num[]' id='serie' min='1' max='999' step='1'><br>";
            } else {
                echo "<input type='number' name='num[]' id='num" . $i . "' min='1' max='49' step='1'>";
            }
        }
        echo "<label for='titulo'>Nombre de tu apuesta (opcional):&nbsp;</label><input type='text' name='titulo'><br>";
        echo "<input type='submit' value='JUEGA'></form>";
        ?>

    </div>
    <div id="footer">
        <hr>
        <p>
            © David Benítez Cabeza - 2ºDAW 2020/21
        </p>
        </hr>
    </div>
</body>

</html>