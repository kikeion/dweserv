<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Extras Tema 6: funciones - Ejercicio 1</title>
</head>

<body>
    <div id="header">
        <h1>Ejercicios Adicionales Tema 6: Funciones</h1>
        <h2>Ejercicio 1: resultado</h2>
    </div>
    <div id="content">
        <?php

        // recogemos los parámetros:
        if (isset($_REQUEST['num'])) {
            $num = $_REQUEST['num'];
            $titulo = $_REQUEST['titulo'];
            if ($titulo == "") {
                $titulo = 'Combinación generada para la Primitiva...';
            }
            $num = combinacion($num);
            imprimeApuesta($num, $titulo);
        }

        //// FUNCIONES ////
        function combinacion($num)
        {
            for ($i = 0; $i < count($num); $i++) {
                if ($num[$i] == "") {
                    do {
                        $n = random_int(1, 49);
                    } while (in_array($n, $num));
                    $num[$i] = $n;
                }
            }
            return $num;
        }
        function imprimeApuesta($num, $titulo)
        {
            echo "<h3 id='title'>" . $titulo . ":</h3><br>";
            echo "<table id='resultado'><tr>";
            for ($i = 0; $i < count($num); $i++) {
                if ($i == (count($num) - 1)) {
                    print_r("<tr><td colspan='6'>Serie:&nbsp;" . $num[$i] . "</td><tr>");
                } else {
                    print_r("<td>" . $num[$i] . "</td>");
                }
            }
            echo "</tr></table>";
        }
        echo "<br><a href='Ejercicio01_extrasT6.php'>VOLVER</a>";

        ?>

    </div>

    </div>
    <div id="footer">
        <hr>
        <p>
            © David Benítez Cabeza - 2ºDAW 2020/21
        </p>
        </hr>
    </div>

</body>

</html>