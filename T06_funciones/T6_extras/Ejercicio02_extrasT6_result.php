<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>E02 Resultado</title>
</head>

<body>

    <?php
    if (isset($_REQUEST['respuesta'])) {
        include 'librerias/controlAcceso.php';
        $perfil = $_REQUEST['profile'];
        $coordenadas = $_REQUEST['coordenadas'];
        $tarjeta = unserialize(base64_decode($_REQUEST['tarjeta']));
        if (compruebaClave($tarjeta, $coordenadas, $_REQUEST['respuesta'])) {
            //echo "<meta http-equiv='refresh' content='3;url=www.google.com>";
            echo "<p><h1>ACCESO PERMITIDO</h1></p>";
            echo "<form action='https://ciclos.iesruizgijon.es'><input type='submit' value='ACCEDER'></form>";
        } else {
            echo "<p><h1>ACCESO DENEGADO</h1></p>";
            if ($perfil == 'admin') {
                echo "<a href='Ejercicio02_extrasT6.php?profile=admin&tarjetaAdmin=" . base64_encode(serialize($tarjeta)) . "' class='button'>VOLVER</a>";
            }
            if ($perfil == 'user') {
                echo "<a href='Ejercicio02_extrasT6.php?profile=user&tarjetaUser=" . base64_encode(serialize($tarjeta)) . "' class='button'>VOLVER</a>";
            }
        }
    } //if
    ?>


</body>

</html>