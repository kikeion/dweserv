<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Extras Tema 6: funciones - Ejercicio 2</title>
    <link rel="stylesheet" href="css/style.css">
    <!-- Disponemos de 2 tarjetas de coordenadas para controlar el acceso a una web. 
    Cada tarjeta corresponde a un perfil de usuario ‘admin’ o ‘estandar’, cada número número registrado en la tarjeta 
    se identifica por su fila (de la 1 a la 5) y su columna (de la A a la E). 
    Los valores registrados en cada tarjeta son fijos y os lo podéis inventar. 
    Crea una página principal que sirva de control de acceso a una segunda página. 
    Se pedirá el perfil de usuario (admin o estándar) y una clave aleatoria correspondiente a la tarjeta de coordenadas 
    de su perfil (fila y columna), se comprobará si es correcto usando 2 funciones: 
    dameTarjeta() a la que se le pasa el perfil y devuelve una matriz correspondiente a la tarjeta de coordenadas de ese perfil, 
    y compruebaClave() a la que se le pasa la matriz de coordenadas, las coordenadas y un valor, y devolverá un booleano según 
    sea correcto el valor en la matriz de coordenadas. 
    Ambas funciones estarán almacenadas en una librería controlAcceso.php. 
    Si el usuario se ha identificado correctamente se muestra un enlace de acceso a la página protegida (cualquiera) y si no 
    mostrará un enlace para volver a intentarlo de nuevo. 
    Usar una tercera función imprimeTarjeta() que recibe una tarjeta y la imprime en una tabla para comprobar el valor de todas 
    las coordenadas. (imprimir las tarjetas de cada perfil en la página de acceso para poder comprobar el correcto funcionamiento 
    de la página) -->
</head>

<body>
    <div id="header">
        <h1>Ejercicios Adicionales Tema 6: Funciones</h1>
        <h2>Ejercicio 2</h2>
    </div>
    <div id="content">
        <?php

        // referenciamos el archivo de funciones:
        include 'librerias/controlAcceso.php';

        // perfil elegido:
        if (isset($_REQUEST['profile'])) {
            $perfil = $_REQUEST['profile'];
            if ($perfil == 'admin') {
                $tarjeta = $_REQUEST['tarjetaAdmin']; // para enviar coordenadas al control de acceso
                $tarjetaAdmin = unserialize(base64_decode($_REQUEST['tarjetaAdmin']));
                echo "<div id='ad'>ADMINISTRADOR:</div>";
                imprimeTarjeta($tarjetaAdmin);
            }
            if ($perfil == 'user') {
                $tarjeta = $_REQUEST['tarjetaUser'];
                $tarjetaUser = unserialize(base64_decode($_REQUEST['tarjetaUser']));
                echo "<div id='us'>USUARIO:</div>";
                imprimeTarjeta($tarjetaUser);
            }
            // GENERAMOS LAS COORDENADAS A SOLICITAR:
            $numero = random_int(1, 5);
            $numLetra = random_int(1, 5);
            $letra = "";
            switch ($numLetra) {
                case 1:
                    $letra = "A";
                    break;
                case 2:
                    $letra = "B";
                    break;
                case 3:
                    $letra = "C";
                    break;
                case 4:
                    $letra = "D";
                    break;
                case 5:
                    $letra = "E";
                    break;
            }
            $coordenadas = $numero . $letra;
            // FORMULARIO DE SOLICITUD DE ACCESO:
        ?>
            <!-- FORMULARIO -->
            <div id="formulario">
                <label for="formulario">Introduce el número en la posición:&nbsp;<b><?= $coordenadas ?></b></label><br>
                <form name="formulario" id="tarjetas" action="Ejercicio02_extrasT6_result.php" method="get">
                    <input type="number" name="respuesta" id="respuesta" min="1" max="99" step="1">
                    <input type="hidden" name="tarjeta" value="<?= $tarjeta ?>">
                    <input type="hidden" name="coordenadas" value="<?= $coordenadas ?>">
                    <input type="hidden" name="profile" value="<?= $perfil ?>">
                    <input type="submit" value="enviar">
                </form>
            </div>

        <?php

        } else {
            // es la primera vez que entramos:

            // ASIGNAMOS VALORES ALEATORIOS A LAS TARJETAS:
            $admin = tarjetaAleatoria();
            $user = tarjetaAleatoria();

            // IMPRIMO LAS TARJETAS:
            echo "<label for='admin'>ADMINISTRADOR:</label>";
            imprimeTarjeta($admin);
            echo "<label for='user'>USUARIO:</label>";
            imprimeTarjeta($user);

        ?>
            <!-- FORMULARIO -->
            <div id="formulario">
                <label for="formulario">Selecciona un perfil:</label>
                <form name="formulario" id="tarjetas" action="Ejercicio02_extrasT6.php" method="get">
                    <select name="profile" id="perfil">
                        <option value="admin">Administrador</option>
                        <option value="user">Usuario</option>
                    </select>
                    <input type="hidden" name="tarjetaAdmin" value="<?= base64_encode(serialize($admin)) ?>">
                    <input type="hidden" name="tarjetaUser" value="<?= base64_encode(serialize($user)) ?>">
                    <input type="submit" value="enviar">
                </form>
            </div>

        <?php
        } // else 
        ?>

    </div>
    <div id="footer">
        <hr>
        <p>
            © David Benítez Cabeza - 2ºDAW 2020/21
        </p>
        </hr>
    </div>
</body>

</html>