<?php

//// FUNCIÓN PARA IMPRIMIR TARJETAS DE COORDENADAS ////

function imprimeTarjeta($coordenadas)
{
    $letras = ["A", "B", "C", "D", "E"];
    echo "<table>";
    for ($i = 0; $i < 6; $i++) {
        if ($i != 0) {
            // primera columna imprime índice de fila:
            echo "<tr><td><b>", $i, "</b></td>";
        } else {
            // celda (0,0) vacía:
            echo "<tr><td></td>";
        }
        for ($j = 0; $j < 5; $j++) {
            if ($i == 0) {
                // primera fila imprime índice de columna:
                echo "<td><b>", $letras[$j], "</b></td>";
            } else {
                echo "<td>", $coordenadas[$i . $letras[$j]], "</td>";
            }
        }
        echo "</tr>";
    }
    echo "</table>";
}

//// FUNCIÓN PARA GENERAR COORDENADAS ALEATORIAS (SIN REPETIR) ////

function tarjetaAleatoria()
{
    $coordenadas = [];
    $letras = ["A", "B", "C", "D", "E"];
    for ($i = 0; $i < 5; $i++) {
        do {
            $n = random_int(1, 99);
        } while (in_array($n, $coordenadas));
        $coordenadas[($i + 1) . $letras[0]] = $n;

        for ($j = 1; $j < 5; $j++) {
            do {
                $n = random_int(1, 99);
            } while (in_array($n, $coordenadas));
            $coordenadas[($i + 1) . $letras[$j]] = $n;
        }
    }
    return $coordenadas;
}

//// FUNCIÓN PARA COMPROBAR LA CLAVE ////

function compruebaClave($tarjeta, $coordenadas, $respuesta)
{
    if ($tarjeta[$coordenadas] == $respuesta) {
        return true;
    } else {
        return false;
    }
}
