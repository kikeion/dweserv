<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Tema 1 ejercicio 5</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
    <!--Ejercicio 5
    Escribe un programa que utilice las variables $x y $y. Asignales los valores 144 y 999 respectivamente.
    A continuación, muestra por pantalla el valor de cada variable, la suma, la resta, la división
    y la multiplicación. -->
</head>

<body>
    <?php
    $x = 144;
    $y = 999;
    // echo $x, "<br>";
    // echo $y, "<br>";
    print_r(get_defined_vars()); // imprime las variables definidas
    echo $x + $y, "<br>";
    echo $x - $y, "<br>";
    echo $x * $y, "<br>";
    echo $x / $y, "<br>";
    ?>

    <hr>
    <h5>Código fuente:</h5>
    <?php
    highlight_string('
    <?php
    $x = 144;
    $y = 999;
    // echo $x, "<br>";
    // echo $y, "<br>";
    print_r(get_defined_vars()); // imprime las variables definidas
    echo $x + $y, "<br>";
    echo $x - $y, "<br>";
    echo $x * $y, "<br>";
    echo $x / $y, "<br>";
    ?>
    ');
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>