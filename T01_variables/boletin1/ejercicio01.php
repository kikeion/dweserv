<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 1 ejercicio 1</title>
    <!--Ejercicio 1
Escribe un programa que muestre tu nombre por pantalla. Utiliza código PHP. -->
</head>

<body>
    <?php
    echo "David Benítez Cabeza";
    ?>
    <br>
    <hr>
    <h5>Código fuente:</h5>
    <?php
    highlight_string('
    <?php
    echo "David Benítez Cabeza";
    ?>
    ');
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>