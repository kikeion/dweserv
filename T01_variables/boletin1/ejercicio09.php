<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>Tema 1 - Ejercicio 9</title>
  <link href="default.css" rel="stylesheet" type="text/css" />
  <!-- Realiza un conversor de pesetas a euros. La cantidad en pesetas que se quiere convertir deberá estar
almacenada en una variable. -->
</head>

<body>
  <?php
  $pesetas = 3000;
  echo $pesetas, " pesetas son ", round($pesetas / 166.386, 2), " euros."; // redondeo a 2 decimales con round()
  ?>
  <!-- muestro el código fuente con la función highlight_string() -->
  <br><br>
  <hr><br>Código fuente:<br>
  <?php
  highlight_string('
            <?php
              $pesetas = 3000;
              echo $pesetas, " pesetas son ", round($pesetas / 166.386, 2), " euros.";
            ?>
          ');
  ?>

  <div id="footer">
    <hr>
    <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
  </div>

</body>

</html>