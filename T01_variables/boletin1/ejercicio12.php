<!DOCTYPE html>
<html>

<head>

	<meta charset="UTF-8">
	<title>Tema 1 - Ejercicio 12</title>
	<link href="default.css" rel="stylesheet" type="text/css" />
	<!-- Igual que el programa anterior, pero esta vez la pirámide debe aparecer invertida, con el vértice
		hacia abajo. -->

</head>

<body>

	<?php
	echo "&nbsp;&nbsp;&nbsp;*********<br>";
	echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br>";
	echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br>";
	echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*&nbsp;&nbsp;*<br>";
	echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br>";
	?>

	<br><br>
	<hr><br>Código fuente:<br>
	<?php
	highlight_string('
            <?php
              echo "&nbsp;&nbsp;&nbsp;*********<br>";
			  echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br>";
			  echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br>";
			  echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*&nbsp;&nbsp;*<br>";
			  echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br>";
        	?>
          ');
	?>

	<div id="footer">
		<hr>
		<p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
	</div>

</body>

</html>