<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>Tema 1 - Ejercicio 10</title>
	<link href="default.css" rel="stylesheet" type="text/css" />
	<!-- Escribe un programa que pinte por pantalla una pirámide rellena a base de asteriscos. La base de la
pirámide debe estar formada por 9 asteriscos. -->

<body>
	<?php
	echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br>";
	echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;***<br>";
	echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*****<br>";
	echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*******<br>";
	echo "&nbsp;&nbsp;&nbsp;&nbsp;*********<br>";
	?>

	<br><br>
	<hr><br>Código fuente:<br>
	<?php
	highlight_string('
            <?php
          		echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br>";
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;***<br>";
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*****<br>";
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*******<br>";
				echo "&nbsp;&nbsp;&nbsp;&nbsp;*********<br>";
        	?>
          ');
	?>

	<div id="footer">
		<hr>
		<p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
	</div>

</body>

</html>