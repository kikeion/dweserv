<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Tema 1 ejercicio 3</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
    <!-- Ejercicio 3.
    Escribe un programa que muestre por pantalla 10 palabras en inglés junto a su correspondiente
traducción al castellano. Las palabras deben estar distribuidas en dos columnas. Utiliza la etiqueta
<table> de HTML. -->
</head>

<body>
    <?php
    echo "<table border='1px' cellpadding='10px' cellspacing='5px'>";
    ?>
    <tr>
        <th>INGLÉS</th>
        <th>ESPAÑOL</th>
    </tr>
    <tr>
        <td>house</td>
        <td>casa</td>
    </tr>
    <tr>
        <td>car</td>
        <td>coche</td>
    <tr>
        <td>cat</td>
        <td>gato</td>
    </tr>
    <?php
    echo "</table>";
    ?>

    <hr>
    <h5>Código fuente:</h5>
    <?php
    highlight_string('
    <?php
    echo "<table border="1px" cellpadding="10px" cellspacing="5px">";
    ?>
    <tr>
        <th>INGLÉS</th>
        <th>ESPAÑOL</th>
    </tr>
    <tr>
        <td>house</td>
        <td>casa</td>
    </tr>
    <tr>
        <td>car</td>
        <td>coche</td>
    <tr>
        <td>cat</td>
        <td>gato</td>
    </tr>
    <?php
    echo "</table>";
    ?>
    ');
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>