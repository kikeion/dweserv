<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Tema 1 ejercicio 4</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
    <!--Ejercicio 4
    Escribe un programa que muestre tu horario de clase mediante una tabla. Aunque se puede hacer
    íntegramente en HTML (igual que los ejercicios anteriores), ve intercalando código HTML y PHP
    para familiarizarte con éste último. -->
</head>

<body>
    <table border='1px' cellpadding='10px' cellspacing='0px'>
        <?php
        echo "<tr align='center'>", "<th>LUNES</th>", "<th>MARTES</th>", "<th>MIÉRCOLES</th>", "<th>JUEVES</th>", "<th>VIERNES</th>", "</tr>";
        echo "<tr align='center'>", "<td>DWEC</td>", "<td>DWEC</td>", "<td>EIE</td>", "<td>DWES</td>", "<td>LC</td>", "</tr>";
        echo "<tr align='center'>", "<td>DWEC</th>", "<td>DWEC</th>", "<td>EIE</td>", "<td>DWES</td>", "<td>LC</td>", "</tr>";
        echo "<tr align='center'>", "<td>DWEC</th>", "<td>DWEC</th>", "<td>DIW</td>", "<td>DWES</td>", "<td>LC</td>", "</tr>";
        echo "<tr align='center'>", "<td>DWES</th>", "<td>DIW</th>", "<td>DIW</td>", "<td>DIW</td>", "<td>DAW</td>", "</tr>";
        echo "<tr align='center'>", "<td>DWES</th>", "<td>DIW</th>", "<td>DWES</td>", "<td>EIE</td>", "<td>DAW</td>", "</tr>";
        echo "<tr align='center'>", "<td>DWES</th>", "<td>DIW</th>", "<td>DWES</td>", "<td>EIE</td>", "<td>DAW</td>", "</tr>";
        ?>
    </table>

    <hr>
    <h5>Código fuente:</h5>
    <?php
    highlight_string('
    <?php
        echo "<tr align="center">", "<th>LUNES</th>", "<th>MARTES</th>", "<th>MIÉRCOLES</th>", "<th>JUEVES</th>", "<th>VIERNES</th>", "</tr>";
        echo "<tr align="center">", "<td>DWEC</td>", "<td>DWEC</td>", "<td>EIE</td>", "<td>DWES</td>", "<td>LC</td>", "</tr>";
        echo "<tr align="center">", "<td>DWEC</th>", "<td>DWEC</th>", "<td>EIE</td>", "<td>DWES</td>", "<td>LC</td>", "</tr>";
        echo "<tr align="center">", "<td>DWEC</th>", "<td>DWEC</th>", "<td>DIW</td>", "<td>DWES</td>", "<td>LC</td>", "</tr>";
        echo "<tr align="center">", "<td>DWES</th>", "<td>DIW</th>", "<td>DIW</td>", "<td>DIW</td>", "<td>DAW</td>", "</tr>";
        echo "<tr align="center">", "<td>DWES</th>", "<td>DIW</th>", "<td>DWES</td>", "<td>EIE</td>", "<td>DAW</td>", "</tr>";
        echo "<tr align="center">", "<td>DWES</th>", "<td>DIW</th>", "<td>DWES</td>", "<td>EIE</td>", "<td>DAW</td>", "</tr>";
        ?>
    ');
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>