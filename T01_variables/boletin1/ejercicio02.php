<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 1 ejercicio 2</title>
    <!--Ejercicio 2
Modifica el programa anterior para que muestre tu dirección y tu número de teléfono. Cada dato
se debe mostrar en una línea diferente. Mezcla de alguna forma las salidas por pantalla, utilizando
tanto HTML como PHP. -->
</head>

<body>
    <?php
    echo "David Benítez Cabeza", "<br>";
    echo "Avenida Fernanda y Bernarda, 3", "<br>";
    ?>
    675466760<br>

    <hr>
    <h5>Código fuente:</h5>
    <?php
    highlight_string('
    <?php
    echo "David Benítez Cabeza", "<br>";
    echo "Avenida Fernanda y Bernarda, 3", "<br>";
    ?>
    ');
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>