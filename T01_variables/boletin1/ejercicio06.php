<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>Tema 1 - Ejercicio 6</title>
	<link href="default.css" rel="stylesheet" type="text/css" />
	<!-- Crea la variable $nombre y asígnale tu nombre completo. Muestra su valor por pantalla de tal forma
que el resultado sea el mismo que el del ejercicio 1. -->
</head>

<body>
	<?php
	$nombre = "David Benítez Cabeza";
	echo $nombre;
	?>

	<hr>
	<h5>Código fuente:</h5>
	<?php
	highlight_string('
    <?php
	$nombre = "David Benítez Cabeza";
	echo $nombre;
	?>
    ');
	?>

	<div id="footer">
		<hr>
		<p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
	</div>

</body>

</html>