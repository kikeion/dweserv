<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Tema 1 - Ejercicio 7</title>
	<link href="default.css" rel="stylesheet" type="text/css" />
	<!-- Crea las variables $nombre, $direccion y $telefono y asígnales los valores adecuados. Muestra los
valores por pantalla de tal forma que el resultado sea el mismo que el del ejercicio 2. -->
</head>

<body>

	<?php
	$nombre    = "David Benítez Cabeza";
	$direccion = "Avenida Fernanda y Bernarda 3";
	$telefono  = "675466760";
	echo $nombre, "<br>";
	echo $direccion, "<br>";
	echo $telefono;
	?>

	<hr>
	<h5>Código fuente:</h5>
	<?php
	highlight_string('
    <?php
	$nombre    = "David Benítez Cabeza";
	$direccion = "Avenida Fernanda y Bernarda 3";
	$telefono  = "675466760";
	echo $nombre, "<br>";
	echo $direccion, "<br>";
	echo $telefono;
	?>
    ');
	?>

	<div id="footer">
		<hr>
		<p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
	</div>

</body>

</html>