<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link href="default.css" rel="stylesheet" type="text/css">
    <title>
        Ejercicio 10 - Boletín 4
    </title>
    <!-- Escribe un programa que calcule la media de un conjunto de números positivos introducidos por
        teclado. A priori, el programa no sabe cuántos números se introducirán. El usuario indicará que ha
        terminado de introducir los datos cuando meta un número negativo. -->
</head>

<body>
    <header id="titulo">
        <h2>
            Tema 4 Bucles: Ejercicio 10
        </h2>
        <h3>Cálculo de la media de los números introducidos</h3>
    </header>
    <div id="content">

    <?php
$numero    = $_POST["numero"];
$sumatorio = $_POST["sumatorio"];
$contador  = $_POST["contador"];

if (!isset($numero) || ($numero > 0)) {
    // si no recibe $numero ó no es un número negativo: lo va sumando y contando las veces:
    $sumatorio += $numero;
    $contador++;
    ?>
            <form action="ejercicio10.php" method="post">
                <label for="formulario">Introduce un número positivo (introduce un número negativo para finalizar):</label>
                <input type="number" name="numero" autofocus required>
                <input type="hidden" name="sumatorio" value="<?php echo $sumatorio; ?>">
                <input type="hidden" name="contador" value="<?php echo $contador; ?>">
                <input type="submit" value="ACEPTAR">
            </form>
        <?php
} else {
    $numero    = $_POST["numero"];
    $sumatorio = $_POST["sumatorio"];
    $contador  = $_POST["contador"];
    ?>
            <br><br>La media de los números introducidos es <?php echo $sumatorio / ($contador - 1); ?>
            <br><br>
            <a href="ejercicio10.php">Volver</a>
        <?php
}
?>

    </div>
    <div class="codigo_fuente">
        <br>
        <h5>Código fuente:</h5>
        <?php
highlight_string('

        ');
?>
    </div>
    <div id="footer">
        <hr>
        <p>
            © David Benítez Cabeza - 2ºDAW 2020/21
        </p>
        </hr>
    </div>
</body>

</html>