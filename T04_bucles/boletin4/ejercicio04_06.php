<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link href="default.css" rel="stylesheet" type="text/css">
    <title>
        Ejercicios 4 a 6 - Boletín 4
    </title>
</head>

<body>
    <header id="titulo">
        <h2>
            Tema 4 Bucles: Ejercicios 4 a 6
        </h2>
    </header>
    <div id="content">
        <?php
        echo "<h4>4. Muestra los números del 320 al 160, contando de 20 en 20 utilizando un bucle FOR:</h4>";
        for ($num = 320; $num > 159; $num -= 20) {
            echo $num, " | ";
        }
        echo "<h4>5. Muestra los números del 320 al 160, contando de 20 en 20 utilizando un bucle WHILE:</h4>";
        $num = 320;
        while ($num > 159) {
            echo $num, " | ";
            $num -= 20;
        }
        echo "<h4>6. Muestra los números del 320 al 160, contando de 20 en 20 utilizando un bucle DO-WHILE:</h4>";
        $num = 320;
        do {
            echo $num, " | ";
            $num -= 20;
        } while ($num > 159);
        ?>
    </div>
    <div class="codigo_fuente">
        <br>
        <h5>Código fuente:</h5>
        <?php
        highlight_string('
		<?php
        echo "<h4>4. Muestra los números del 320 al 160, contando de 20 en 20 utilizando un bucle FOR:</h4>";
        for ($num = 320; $num > 159; $num -= 20) {
            echo $num, " | ";
        }
        echo "<p>5. Muestra los números del 320 al 160, contando de 20 en 20 utilizando un bucle WHILE:</p>";
        $num = 320;
        while ($num > 159) {
            echo $num, " | ";
            $num -= 20;
        }
        echo "<h4>6. Muestra los números del 320 al 160, contando de 20 en 20 utilizando un bucle DO-WHILE:</h4>";
        $num = 320;
        do {            
            echo $num, " | ";
            $num -= 20;
        } while ($num > 159);
        ?>     
        ');
        ?>
    </div>
    <div id="footer">
        <hr>
        <p>
            © David Benítez Cabeza - 2ºDAW 2020/21
        </p>
        </hr>
    </div>
</body>

</html>