<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link href="default.css" rel="stylesheet" type="text/css">
    <title>
        Ejercicio 11 - Boletín 4
    </title>
    <!-- Escribe un programa que muestre en tres columnas, el cuadrado y el cubo de los 5 primeros números
                        enteros a partir de uno que se introduce por teclado. -->
    </link>
    </meta>
    </meta>
</head>

<body>
    <header id="titulo">
        <h2>
            Tema 4 Bucles: Ejercicio 11
        </h2>
        <h3>
            Cuadrado y cubo de los 5 primeros números enteros a partir de uno introducido por teclado:
        </h3>
    </header>
    <div id="content">
        <?php
        $num    = $_POST["numero"];

        if (!isset($num)) {
            // si no recibe número: lo va sumando y contando las veces:
        ?>
            <form action="ejercicio11.php" method="post">
                <label for="formulario">
                    Introduce un número entero:
                </label>
                <input type="number" name="numero"  autofocus required >
                <input type="submit" value="ACEPTAR">
            </form>
        <?php
        } else {
        ?>
            <?php echo "<table><tr><th>numero</th><th>número<sup>2</sup></th><th>número<sup>3</sup></th></tr>;
            
            
            
            
            </table>" ?>
            <br>
            <br>
            <a href="ejercicio10.php">
                Volver
            </a>
        <?php
        }
        ?>
        </br>
        </br>
        </br>
        </br>
    </div>
    <div class="codigo_fuente">
        <br>
        <h5>
            Código fuente:
        </h5>
        <?php
        highlight_string('
        <?php
        if (!isset($_POST["numIntroducido"])) {
            $numIntroducido = 0; // le doy un valor por defecto si no recoje el parámetro
        } else {
            $numIntroducido = $_POST["numIntroducido"];
            echo "<table id="tabla_multi">
                ";
            for ($i = 1; $i < 11; $i++) {
                echo "
                <tr>
                    <td>
                        ", $numIntroducido, " x ", $i, "
                    </td>
                    ";
                for ($j = 0; $j < 1; $j++) {
                    echo "
                    <td>
                        ", ($numIntroducido * $i), "
                    </td>
                </tr>
                ";
                }
            }
            echo "
            </br>
        </div>
    </body>
</html>
";
        }
        ?>
        ');
        ?>
        <div id="footer">
            <hr>
            <p>
                © David Benítez Cabeza - 2ºDAW 2020/21
            </p>
            </hr>
        </div>