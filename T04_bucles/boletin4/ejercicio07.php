<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link href="default.css" rel="stylesheet" type="text/css">
    <title>
        Ejercicio 7 - Boletín 4
    </title>
    <!-- Realiza el control de acceso a una caja fuerte. La combinación será un número de 4 cifras. El
        programa nos pedirá la combinación para abrirla. Si no acertamos, se nos mostrará el mensaje
        “Lo siento, esa no es la combinación” y si acertamos se nos dirá “La caja fuerte se ha abierto
        satisfactoriamente”. Tendremos cuatro oportunidades para abrir la caja fuerte. -->
</head>

<body>
    <header id="titulo">
        <h2>
            Tema 4 Bucles: Ejercicio 7
        </h2>
    </header>
    <div id="content">

        <?php
        if (!isset($_POST['oportunidades'])) {
            $claveIntroducida = 00000; // le doy una clave por defecto si no recoje $oportunidades
            $oportunidades = 4;
        } else {
            $oportunidades = $_POST['oportunidades'];
            $claveIntroducida = $_POST['claveIntroducida'];
        }
        $claveSecreta = 1234;
        if ($claveIntroducida == $claveSecreta) {
            echo "  <script type='text/javascript'>
                                    alert('La caja fuerte se ha abierto satisfactoriamente');
                                    window.location.href='ejercicio07.php';
                                </script>";
        } else if ($oportunidades == 0) {
            echo "Lo siento, has agotado todas tus oportunidades.";
        } else {
            echo "Te quedan ", $oportunidades, " oportunidades para abrir la caja fuerte.<br>";
            $oportunidades--;
        }
        ?>

        <p>Introduce un número de cuatro cifras:</p>
        <form action="ejercicio07.php" id="respuesta" method="post">
            <label for="respuesta">Contraseña:</label>
            <input type="number" name="claveIntroducida" min=0 max=9999 autofocus required id="claveIntroducida">
            <input type="hidden" name="oportunidades" value="<?= $oportunidades ?>">
            <input type="submit" value="ACEPTAR">
        </form>

    </div>
    <div class="codigo_fuente">
        <br>
        <h5>Código fuente:</h5>
        <?php
        highlight_string('
		<?php
        if (!isset($_POST["oportunidades"])) {
            $claveIntroducida = 00000; // le doy una clave por defecto si no recoje $oportunidades
            $oportunidades = 4;
        } else {
            $oportunidades = $_POST["oportunidades"];
            $claveIntroducida = $_POST["claveIntroducida"];
        }
        $claveSecreta = 1234;
        if ($claveIntroducida == $claveSecreta) {
            echo "  <script type="text/javascript">
                                    alert("La caja fuerte se ha abierto satisfactoriamente");
                                    window.location.href="ejercicio07.php";
                                </script>";
        } else if ($oportunidades == 0) {
            echo "Lo siento, has agotado todas tus oportunidades.";
        } else {
            echo "Te quedan ", $oportunidades, " oportunidades para abrir la caja fuerte.<br>";
            $oportunidades--;
        }
        ?>   
        ');
        ?>
    </div>
    <div id="footer">
        <hr>
        <p>
            © David Benítez Cabeza - 2ºDAW 2020/21
        </p>
        </hr>
    </div>
</body>

</html>