<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="default.css">
	<title>Ejercicios 1 a 3 - Boletín 4</title>
	<!-- Muestra los números múltiplos de 5 de 0 a 100 utilizando un bucle for. -->
</head>

<body>
	<header id="titulo">
		<h2>Tema 4 Bucles: Ejercicios 1 a 3</h2>
	</header>
	<div id="content">
		<?php
		echo "<h4>1. Múltiplos de 5 de 0 a 100 con bucle FOR:</h4>";
		for ($i = 0; $i < 21; $i++) {
			echo 5 * $i, " | ";
		}
		echo "<h4>2. Múltiplos de 5 de 0 a 100 con bucle WHILE:</h4>";
		$i = 0;
		while ($i < 21) {
			echo 5 * $i, " | ";
			$i++;
		}
		echo "<h4>3. Muestra los números múltiplos de 5 de 0 a 100 utilizando un bucle DO-WHILE:</h4>";
		$i = 0;
		do {
			echo 5 * $i, " | ";
			$i++;
		} while ($i < 21);
		?>
	</div>
	<div class="codigo_fuente">
		<br>
		<h5>Código fuente:</h5>
		<?php
		highlight_string('
		<?php
		echo "<h4>1. Múltiplos de 5 de 0 a 100 con bucle FOR</h4>";
		for ($i = 0; $i < 21; $i++) {
			echo 5 * $i, " | ";
		}
		echo "<h4>2. Múltiplos de 5 de 0 a 100 con bucle WHILE</h4>";
		$i = 0;
        while ($i < 21) {
            echo 5 * $i, " | ";
            $i++;
		}
		echo "<h4>3. Muestra los números múltiplos de 5 de 0 a 100 utilizando un bucle do-while.</h4>";
		$i = 0;
        do {
            echo 5 * $i, " | ";
            $i++;
        } while ($i < 21);
		?>       
        ');
		?>
	</div>
	<div id="footer">
		<hr>
		<p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
	</div>
</body>

</html>