<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link href="default.css" rel="stylesheet" type="text/css">
    <title>
        Ejercicio 8 - Boletín 4
    </title>
    <!-- Muestra la tabla de multiplicar de un número introducido por teclado. El resultado se debe mostrar
        en una tabla (<table> en HTML). -->
</head>

<body>
    <header id="titulo">
        <h2>
            Tema 4 Bucles: Ejercicio 8
        </h2>
    </header>
    <div id="content">

        <?php
        if (!isset($_POST['numIntroducido'])) {
            $numIntroducido = 0; // le doy un valor por defecto si no recoje el parámetro
        } else {
            $numIntroducido = $_POST['numIntroducido'];
            echo "<table id='tabla_multi'>";
            for ($i = 1; $i < 11; $i++) {
                echo "<tr><td>", $numIntroducido, " x ", $i, "</td>";
                for ($j = 0; $j < 1; $j++) {
                    echo "<td>", ($numIntroducido * $i), "</td></tr>";
                }
            }
            echo "</table>";
        }
        ?>
        <p>Introduce un número para mostrar su tabla de multiplicar:</p>
        <form action="ejercicio08.php" id="tabla" method="post">
            <label for="tabla">Número:</label>
            <input type="number" name="numIntroducido" min=1 max=10 autofocus required id="numIntroducido">
            <input type="submit" value="ACEPTAR">
        </form>

    </div>
    <div class="codigo_fuente">
        <br>
        <h5>Código fuente:</h5>
        <?php
        highlight_string('
		<?php
        if (!isset($_POST["numIntroducido"])) {
            $numIntroducido = 0; // le doy un valor por defecto si no recoje el parámetro
        } else {
            $numIntroducido = $_POST["numIntroducido"];
            echo "<table id="tabla_multi">";
            for ($i = 1; $i < 11; $i++) {
                echo "<tr><td>", $numIntroducido, " x ", $i, "</td>";
                for ($j = 0; $j < 1; $j++) {
                    echo "<td>", ($numIntroducido * $i), "</td></tr>";
                }
            }
            echo "</table>";
        }
        ?>
        ');
        ?>
    </div>
    <div id="footer">
        <hr>
        <p>
            © David Benítez Cabeza - 2ºDAW 2020/21
        </p>
        </hr>
    </div>
</body>

</html>