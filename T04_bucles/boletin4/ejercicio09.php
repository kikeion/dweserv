<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link href="default.css" rel="stylesheet" type="text/css">
    <title>
        Ejercicio 9 - Boletín 4
    </title>
    <!-- Realiza un programa que nos diga cuántos dígitos tiene un número introducido por teclado. -->
</head>

<body>
    <header id="titulo">
        <h2>
            Tema 4 Bucles: Ejercicio 9
        </h2>
    </header>
    <div id="content">

        <?php
        if (!isset($_POST["numero"])) {
            $numero = 0;
            $cifras = 0;
        } else {
            $numero = $_POST["numero"];
            $cifras = 1;

            while ($numero > 9) {
                $numero = floor($numero / 10);
                $cifras++;
            }
            echo "<span><b>El número ", $_POST["numero"], " tiene ", $cifras, " cifras</b></span>";
        }
        ?>
        <form action="ejercicio09.php" id="formulario" method="post">
            <label for="formulario">Introduce un número:</label>
            <input type="number" name="numero" min=0 autofocus required id="numero">
            <input type="submit" value="ACEPTAR">
        </form>

    </div>
    <div class="codigo_fuente">
        <br>
        <h5>Código fuente:</h5>
        <?php
        highlight_string('
		<?php
        if (!isset($_POST["numero"])) {
            $numero = 0;
            $cifras = 0;
        } else {
            $numero = $_POST["numero"];
            $cifras = 1;

            while ($numero > 9) {
                $numero = floor($numero / 10);
                $cifras++;
            }
            echo "<span><b>El número ", $_POST["numero"], " tiene ", $cifras, " cifras</b></span>";
        }
        ?>
        ');
        ?>
    </div>
    <div id="footer">
        <hr>
        <p>
            © David Benítez Cabeza - 2ºDAW 2020/21
        </p>
        </hr>
    </div>
</body>

</html>