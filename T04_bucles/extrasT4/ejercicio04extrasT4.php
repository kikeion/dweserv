<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 4 - Extras T4</title>
    <link rel="stylesheet" href="css/default1.css" type="text/css">
    <!-- Realiza el ejercicio 2 del tema anterior correspondiente al juego de la primitiva, pero usando
        estructuras repetitivas para simplificar el código. -->
</head>

<body>
    <p>Introduce tu apuesta:</p>
    <form id=formulario action="ejercicio04extrasT4_result.php" method="POST">
        <fieldset label="numeros">
            <legend>Tu apuesta:</legend>
            <table>
                <?php
                for ($i = 1; $i < 26; $i++) {
                    echo "<tr><td><label for='", $i, "'>", $i, "</label><input type='checkbox' name='", $i, "' id='numeros'></td>";
                    for ($j = 1; $j < 2; $j++) {
                        if ($i !== 25) {
                            echo "<td><label for='", ($i + 25), "'>", ($i + 25), "</label><input type='checkbox' name='", ($i + 25), "' id='numeros'></td></tr>";
                        }                         
                    }                    
                }
                echo "<td><label for='serie'>Serie</label><input type='number' name='serie' min='0' max='1000' step='1' id='serie' required></td></tr>";
                ?>
            </table>
        </fieldset><br>
        <input type="submit" value="JUEGA!">
    </form>
    <br><br>
    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>