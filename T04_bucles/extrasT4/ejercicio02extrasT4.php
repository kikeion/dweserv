<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 2 - Extras Tema 4</title>
    <link rel="stylesheet" href="css/default.css">
    <!-- Diseñar una página que esté compuesta por una tabla de 100 filas por 100 columnas, y en cada
        celda habrá una imagen de un ojo cerrado. Cada vez que el usuario pulse un ojo, ser recargará
        la página con todos los ojos cerrados salvo el que se ha pulsado que corresponderá a un ojo
        abierto. -->
    <?php
    // Recoge el parámetro que envía la imagen:
    if (isset($_REQUEST["seleccion"])) {
        $sel = $_REQUEST["seleccion"];
        echo "<meta http-equiv='refresh' content='2; url=ejercicio02extrasT4.php'>";
    }
    ?>
</head>

<body>

    </form>

    <?php
    // en páginas que recargan datos siempre pedir al principio si es la primera vez que se pide el parámetro:
    if (isset($_REQUEST['seleccion'])) {
        $sel = $_REQUEST['seleccion'];
    } else {
        $sel = 0;
    }

    $n = 0; // controla la posición de cada celda

    echo "<table id= 'ojos'>";
    //imprimo ojos cerrados
    for ($i = 1; $i < 10; $i++) {
        $n++;
        if ($sel == $n) {
            echo "<tr><td><a href='ejercicio02extrasT4.php?seleccion=$n'><img src='img/openeye.png' width='50px'></td>";
        } else {
            echo "<tr><td><a href='ejercicio02extrasT4.php?seleccion=$n'><img src='img/closedeye.png' width='50px'></td>";
        }
        for ($j = 1; $j < 10; $j++) {
            $n++;
            if ($sel == $n) {
                echo "<td><a href='ejercicio02extrasT4.php?seleccion=$n'><img src='img/openeye.png' width='50px'></td>";
            } else {
                echo "<td><a href='ejercicio02extrasT4.php?seleccion=$n'><img src='img/closedeye.png' width='50px'></td>";
            }
        }
    }
    echo "</table>";
    ?>

</body>

</html>