<!DOCTYPE html>
<html lang="es">
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/default1.css" rel="stylesheet" type="text/css" />
    <title>Extras Tema 4 - Ejercicio 4: resultado</title>
</head>

<body>

    <?php
    $num1 = random_int(1, 49);
    $num2 = random_int(1, 49);
    $num3 = random_int(1, 49);
    $num4 = random_int(1, 49);
    $num5 = random_int(1, 49);
    $num6 = random_int(1, 49);
    $serie = random_int(0, 1000);
    $numerosAcertados ="";
    $aciertos = 0;
    $premio = 0;
    ?>

    <table border="1">
        <tr>
            <td colspan="6">COMBINACIÓN GANADORA</td>
            <td>SERIE</td>
        </tr>
        <tr>
            <td><?= $num1 ?></td>
            <td><?= $num2 ?></td>
            <td><?= $num3 ?></td>
            <td><?= $num4 ?></td>
            <td><?= $num5 ?></td>
            <td><?= $num6 ?></td>
            <td><?= $serie ?></td>
        </tr>
    </table>

    <?php
    if (isset($_REQUEST[$num1])) {
        $aciertos++;
        $numerosAcertados .= $num1." "; // lo añado (concateno con un  punto) a la cadena de números acertados (TIPO STRING!)
    }
    if (isset($_REQUEST[$num2])) {
        $aciertos++;
        $numerosAcertados .= $num2." "; // lo añado (concateno con un  punto) a la cadena de números acertados (TIPO STRING!)
    }
    if (isset($_REQUEST[$num3])) {
        $aciertos++;
        $numerosAcertados .= $num3." "; // lo añado (concateno con un  punto) a la cadena de números acertados (TIPO STRING!)
    }
    if (isset($_REQUEST[$num4])) {
        $aciertos++;
        $numerosAcertados .= $num4." "; // lo añado (concateno con un  punto) a la cadena de números acertados (TIPO STRING!)
    }
    if (isset($_REQUEST[$num5])) {
        $aciertos++;
        $numerosAcertados .= $num5." "; // lo añado (concateno con un  punto) a la cadena de números acertados (TIPO STRING!)
    }
    if (isset($_REQUEST[$num6])) {
        $aciertos++;
        $numerosAcertados .= $num6." "; // lo añado (concateno con un  punto) a la cadena de números acertados (TIPO STRING!)
    }
    echo "<p>Número de aciertos: ", $aciertos, "</p><br>";
    echo "<p>Números acertados: ", $numerosAcertados, "</p>";
    switch ($aciertos) {
        case 0:
            if ($_REQUEST['serie'] == $serie) {
                $premio + 500;
            } else {
                echo "<p>Lo siento, no has ganado ni lo metido!</p>";
            }
            break;
        case 1:
            if ($_REQUEST['serie'] == $serie) {
                $premio + 500;
            } else {
                echo "<p>Lo siento, no has ganado ni lo metido!</p>";
            }
            break;
        case 2:
            if ($_REQUEST['serie'] == $serie) {
                $premio + 500;
            } else {
                echo "<p>Lo siento, no has ganado ni lo metido!</p>";
            }
            break;
        case 3:
            if ($_REQUEST['serie'] == $serie) {
                $premio + 500;
            } else {
                echo "<p>Lo siento, no has ganado ni lo metido!</p>";
            }
            break;
        case 4:
            if ($_REQUEST['serie'] == $serie) {
                $premio + 500;
            } else {
                echo "<p>Enhorabuena! Has ganado lo metido!</p>";
            }
            break;
        case 5:
            $premio = 30;
            if ($_REQUEST['serie'] == $serie) {
                $premio + 500;
            }
            echo "<p>Enhorabuena! Has ganado ", $premio, " euros</p>";
            break;
        case 6:
            $premio = 100;
            if ($_REQUEST['serie'] == $serie) {
                $premio + 500;
            }
            echo "<p>Enhorabuena! Has ganado ", $premio, " euros</p>";
            break;
    }
    ?>
    <!---menos de 4 aciertos: nada
    -4 aciertos: dinero vuelto
    -5 aciertos: 30 euros
    -6 aciertos: 100 euros
    -número de serie: Si se acierta se sumarán 500 euros independientemente del número de
    aciertos.-->


    <br>
    <br>
    <button type="button" onclick="location.href='ejercicio04extrasT4.php'">VOLVER A JUGAR</button> <!-- botón volver -->
    <br><br>
    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>