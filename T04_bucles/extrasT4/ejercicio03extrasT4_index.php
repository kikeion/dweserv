<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <title>Ejercicio 1 - Extras Tema 3</title>
    <!-- Realiza el ejercicio 1 del tema anterior correspondiente al juego de adivinar una imagen, pero
        usando estructuras repetitivas para simplificar el código. -->
    <link rel="stylesheet" href="css/default2.css" type="text/css">

</head>

<body>

    <?php 
    if (!isset($_REQUEST['intentos'])) {        
        $intentos = 0; // primera vez que abro la página los intentos = 0
    } else {
        $intentos = $_REQUEST['intentos'];
    }
    ?>

    <p><b>Adivina la imagen</b></p>
    
    <table id="mosaico">        
    <?php 
    $n = 9; // controlo el número de trozo
    
        for ($i =0; $i < 3; $i++) {
            echo "<tr><td><a href='ejercicio03extrasT4.php?trozo=$n&intentos=$intentos'><img src='img/cuadradoverde.png'></a></td>";
            $n--;
            for ($j = 0; $j < 2; $j++) {
                echo "<td><a href='ejercicio03extrasT4.php?trozo=$n&intentos=$intentos'><img src='img/cuadradoverde.png'></a></td>";
                $n--;
            }
            echo "</tr>";            
        }
    ?>
    </table>
    

    <form name="respuesta" action="ejercicio03extrasT4.php" method="post">
        <label for="respuesta">Solución:</label>
        <input type="text" name="solucion" required>
        <input type="hidden" name="intentos" value="<?$intentos?>">
        <input type="submit" name="submit" value="Adivinar">
    </form>

    <?php 
        echo "Llevas ", $intentos, " intentos";
    ?>
    
    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>