<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <title>Ejercicio 3 - Extras Tema 4</title>
    <!-- Realiza el ejercicio 1 del tema anterior correspondiente al juego de adivinar una imagen, pero
        usando estructuras repetitivas para simplificar el código.
        -->
    <link rel="stylesheet" href="css/default2.css" type="text/css">
    <?php

    // intentos:
    if (!isset($_REQUEST['intentos'])) {
        $intentos = 0;
    } else {
        $intentos = $_REQUEST['intentos'];
        $intentos++; // si recoge parámetro aumenta intentos en 1
    }
    // Recoge el parámetro que envía la imagen:
    if (isset($_REQUEST["trozo"])) {
        $trozo = $_REQUEST["trozo"];
        echo "<meta http-equiv='refresh' content='2; url=ejercicio03extrasT4_index.php?intentos=$intentos'>"; // envío intentos
    }
    ?>
</head>

<body>

    <p><b>Adivina la imagen</b></p>

    <?php

    // Compruebo si el parámetro que viene lo hace desde el formulario o desde la imagen:

    // SI VIENE DEL FORMULARIO:

    if (isset($_POST['submit'])) {
        $respuesta = $_POST['solucion'];
        $intentos = $_REQUEST['intentos'];
        if (strtolower($respuesta) == "koala") {
            echo "<p><b>ENHORABUENA, HAS ACERTADO!</b></p>";
            echo "<p><b>TOTAL INTENTOS = ", $intentos, "</b></p>";
            echo "<img src='img/koala-durmiendo.jpg'>";
            echo "<p><a href='ejercicio03extrasT4_index.php'>VOLVER</a></p>";
        } else {
            echo "<p><b>LO SIENTO, NO LO HAS ADIVINADO. SIGUE INTENTÁNDOLO</b></p>";
            echo "<p><a href='ejercicio03extrasT4_index.php?intentos=.$intentos.'>VOLVER</a></p>";
        }

        // SI VIENE DE LA IMAGEN:

    } else {
        echo "<table id='mosaico'>";

        $n = 9; // controlo el número de trozo

        for ($i = 0; $i < 3; $i++) {
            if (isset($trozo) && $trozo == $n) {
                echo "<tr><td><img src='img/", $n, ".jpg'></td>";
            } else {
                echo "<tr><td><img src='img/cuadradoverde.png'></td>";
            }
            $n--;
            for ($j = 0; $j < 2; $j++) {
                if (isset($trozo) && $trozo == $n) {
                    echo "<td><img src='img/", $n, ".jpg'></td>";
                } else {
                    echo "<td><img src='img/cuadradoverde.png'></td>";
                }
                $n--;
            }
            echo "</tr>";
        }
        echo "</table>";
    }
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>