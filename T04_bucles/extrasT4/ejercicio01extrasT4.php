<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 1 - Extras Tema 4</title>
    <link rel="stylesheet" href="css/default.css">
    <!-- Diseñar una página web que muestre una tabla con 3 columnas, la primera corresponde al
        número de bloque, la segunda al piso y en la tercera hay un botón llamar. En total hay 10
        bloques y cada bloque tiene 7 pisos. Cuando se pulse llamar en un piso de un bloque, mostrará
        en otra página el mensaje del tipo “Usted ha llamado al piso 3 del bloque 6”. Utiliza
        estructuras repetitivas para generar la tabla. -->
</head>

<body>

    <?php
 
    if (!isset($_REQUEST['piso']) && !isset($_REQUEST['piso'])) {        
        
    } else {
        $piso = $_POST['piso'];
        $bloque = $_POST['bloque'];
        echo "<div id='telefonillo'>Usted ha llamado al piso ", $piso, " del bloque ", $bloque, "</div><br>";
    }
    
    // encabezado tabla pisos:
    echo "<table><tr><th>Bloque</th><th>Piso</th><th>Llamar</th></tr>";
    // resto de filas y columnas:
    for ($i = 1; $i < 11; $i++) {
        // primera fila:
        echo "<tr><td rowspan='7'>Bloque ", $i, "</td><td>piso1</td>
        <td><form action='ejercicio01extrasT4.php' method='post'>
        <input type='hidden' name='piso' value='1'>
        <input type='hidden' name='bloque' value='$i'>
        <input type='submit' value='llamar'></form>
        </td></tr>";
        // filas siguientes:
        for ($j = 2; $j < 8; $j++) {
            echo "<tr><td>piso ", $j, "</td>
            <td><form action='ejercicio01extrasT4.php' method='post'>
            <input type='hidden' name='piso' value='$j'>
            <input type='hidden' name='bloque' value='$i'>
            <input type='submit' value='llamar'></form>
            </td></tr>";
        }
    }
    echo "</table>";
    ?>    

</body>

</html>