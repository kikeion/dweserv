<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Funciones de texto - Ejercicio 11</title>
    <!-- Escribir una clase que lea n caracteres que forman un número romano y que imprima:  
        a. si la entrada fue correcta, un string que represente el equivalente decimal 
        b. si la entrada fue errónea, un mensaje de error. 
        Nota: La entrada será correcta si contiene solo los caracteres 
        M:1000, D:500, C:100, L:50, X:10, I:1. 
        No se tendrá en cuenta el orden solo se sumará el valor de cada letra.   -->
</head>

<body>
    <div id="content">
        <?php
        if (isset($_REQUEST['texto'])) {
            $texto = (strtoupper(trim($_REQUEST['texto'])));
            echo $texto;
            $romanos = ["I" => 1, "X" => 10, "L" => 50, "C" => 100, "D" => 500, "M" => 1000];
            $correcto = true;
            // comprobamos si la entrada corresponde a nuestros índices romanos:
            for ($i = 0; $i < strlen($texto); $i++) {
                if (!array_key_exists($texto[$i], $romanos)) {
                    $correcto = false;
                    break;
                }
            }
            if ($correcto) {
                $arrayRomano = str_split($texto);
                $suma = 0;
                for ($i = 0; $i < count($arrayRomano); $i++) {
                    if (array_key_exists($arrayRomano[$i], $romanos)) {
                        $suma += $romanos[$arrayRomano[$i]];
                    }
                }
                echo "<p>La suma de los números romanos es: ", $suma, "</p>";
            } else {
                echo "<p>La entrada es incorrecta.</p>";
            }
        }
        ?>
        <label for="formulario">Introduce un número romano:</label>
        <form name="formulario" action="ejercicio11.php" method="post">
            <input type="text" name="texto" required>
            <input type="submit" value="ENVIAR">
        </form>
</body>

</html>