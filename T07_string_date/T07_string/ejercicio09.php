<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Funciones de texto - Ejercicio 9</title>
    <!-- Pedir al usuario una cadena de caracteres e imprimirla invertida. -->
</head>

<body>
    <div id="content">
        <?php
        if (isset($_REQUEST['texto'])) {
            $texto = $_REQUEST['texto'];
            echo $texto, "<br>";
            echo strrev($texto);
            echo "<br><a href='ejercicio09.php'>>>VOLVER</a>";
        } else {
        ?>
            <label for="formulario">Introduce un texto:</label>
            <form name="formulario" action="ejercicio09.php" method="post">
                <input type="text" name="texto" required>
                <input type="submit" value="ENVIAR">
            </form>

        <?php
        } // else
        ?>

</body>

</html>