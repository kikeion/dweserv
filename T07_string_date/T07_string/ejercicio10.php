<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Funciones de texto - Ejercicio 10</title>
    <!-- Escribir un programa que pida un nombre (con sus apellidos) y escriba en pantalla 
    tanto el nombre con las primeras letras en mayúsculas como las iniciales de dicho nombre.  -->
</head>

<body>
    <div id="content">
        <?php
        if (isset($_REQUEST['texto'])) {
            $texto = strtolower($_REQUEST['texto']);
            $arrayNombre = explode(" ", trim($texto));
            foreach ($arrayNombre as $palabra) {
                $palabra = ucfirst(trim($palabra));
                echo $palabra, " ";
            }
            echo "<br>";
            foreach ($arrayNombre as $palabra) {
                $palabra = strtoupper(substr(trim($palabra), 0, 1)) . ". ";
                echo $palabra;
            }
            echo "<br><a href='ejercicio10.php'>>>VOLVER</a>";
        } else {
        ?>
            <label for="formulario">Introduce nombre y apellidos:</label>
            <form name="formulario" action="ejercicio10.php" method="post">
                <input type="text" name="texto" required>
                <input type="submit" value="ENVIAR">
            </form>

        <?php
        } // else
        ?>

</body>

</html>