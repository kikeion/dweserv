<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Funciones de texto - Ejercicio 2</title>
    <!-- Cambiar todas las vocales de la frase “Tengo una hormiguita en la patita, que me esta haciendo 
    cosquillitas y no me puedo aguantar” por otra vocal pedida al usuario.  -->
</head>

<body>
    <div id="content">
        <div id="resultado"></div>
        <?php
        if (isset($_REQUEST['vocal'])) {
            $vocal = strtolower($_REQUEST['vocal']);
            // validación de formulario
            if (preg_match("/[aeiou]/", $vocal)) {
                $cadena = "Tengo una hormiguita en la patita, que me está haciendo cosquillitas y no me puedo aguantar";
                echo "<p><h1>" . str_replace(["a", "e", "i", "o", "u"], $vocal, $cadena) .
                    "</h1></p>";
            } else {
                echo "<p><h1>Tengo una hormiguita en la patita, que me está haciendo cosquillitas y no me puedo aguantar</h1></p>";
                echo "<p>Lo siento, debes introducir una vocal</p>";
            }
        } else {
            echo "<p><h1>Tengo una hormiguita en la patita, que me está haciendo cosquillitas y no me puedo aguantar</h1></p>";
            echo "<br>";
        }
        ?>
        <label for="formulario">Introduce una vocal:</label>
        <form name="formulario" action="ejercicio02.php" method="post">
            <input type="text" maxlength="1" name="vocal" id="vocal">
            <input type="submit" value="CAMBIAR">
        </form>
    </div>

</body>

</html>