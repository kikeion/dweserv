<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Funciones de texto - Ejercicio 4</title>
    <!-- Verificar si un string leído por teclado finaliza con la misma palabra que empieza.   -->
</head>

<body>
    <div id="content">
        <?php
        if (isset($_REQUEST['cadena'])) {
            $cadena = strtolower($_REQUEST['cadena']);
            // elimino el punto final (si lo hubiera):
            if ($cadena[strlen($cadena) - 1] == ".") {
                $cadena[strlen($cadena) - 1] = " ";
            }
            // transformo el string en un array separando por espacios:
            $arrayPalabras = explode(" ", trim($cadena));
            // si el texto tiene una sola palabra o el primer elemento del array es igual al último (siempre que sea string):
            if (is_numeric($cadena)) {
                echo "<p><h2>'", $_REQUEST['cadena'], "' no es un texto.</h2></p>";
            } else if ($arrayPalabras[0] == $arrayPalabras[count($arrayPalabras) - 1]) {
                echo "<p><h2>El texto '<em>", $_REQUEST['cadena'], "</em>' finaliza con la misma palabra que empieza.</h2></p>";
            } else {
                echo "<p><h2>El texto '<em>", $_REQUEST['cadena'], "</em>' NO finaliza con la misma palabra que empieza.</h2></p>";
            }
        }
        ?>
        <label for="formulario">Introduce una frase o palabra:</label>
        <form name="formulario" action="ejercicio04.php" method="post">
            <input type="text" name="cadena" required>
            <input type="submit" value="ENVIAR">
        </form>
</body>

</html>