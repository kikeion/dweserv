<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Funciones de texto - Ejercicio 8</title>
    <!-- Pedir un string al usuario e imprimir todos los números seguidos y sin espacios, correspondientes al código ascii de cada uno de sus caracteres. 
    Posteriormente calcular la frase original a partir de dichos números (usar un array).  -->
</head>

<body>
    <div id="content">
        <?php
        if (isset($_REQUEST['texto'])) {
            $texto = $_REQUEST['texto'];
            $arrayAscii = [];
            echo "<p>ASCII: ";
            // voy metiendo los valores ascii en un array:
            for($i = 0; $i < strlen($texto); $i++) {
                $arrayAscii[$i] = ord($texto[$i]);
                echo $arrayAscii[$i];
            }
            echo "</p>";
            // imprimo cada valor ascii del array en un carácter:
            foreach($arrayAscii as $valor) {
                echo chr($valor);
            }          
            echo "<br><a href='ejercicio08.php'>>>VOLVER</a>";
        } else {
        ?>
            <label for="formulario">Introduce un texto:</label>
            <form name="formulario" action="ejercicio08.php" method="post">
                <input type="text" name="texto" required>
                <input type="submit" value="ENVIAR">
            </form>

        <?php
        } // else
        ?>

</body>

</html>