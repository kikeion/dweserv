<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Funciones de texto - Ejercicio 3</title>
    <!-- Contar cuantas palabras tiene una frase. Recuerde que la gramática establece 
        que una frase termina en un punto.   -->
</head>

<body>
    <div id="content">
        <div id="resultado"></div>
        <?php
        if (isset($_REQUEST['frase'])) {
            $frase = $_REQUEST['frase'];
            // elimino espacios al principio y al final de la frase:
            $frase = trim($frase);
            // añado un punto al final si no lo hubiera:
            if ($frase[strlen($frase) - 1] != ".") {
                $frase = $frase . ".";
            }
            // generamos array cortando por los espacios:
            $arrayPalabras = explode(" ", $frase);
            // contamos los elementos != vacío:
            $cuentaPalabras = 0;
            foreach ($arrayPalabras as $elemento) {
                $elemento = trim($elemento);
                if ($elemento != "") {
                    $cuentaPalabras++;
                }
            }
            echo "<p>" . $frase . "</p>";
            echo "<p>La frase contiene " . $cuentaPalabras . " palabras.</p>";
        }
        ?>
        <form action="ejercicio03.php" method="post">
            <label for="frase">Escribe una frase:</label>
            <input type="text" name="frase" id="frase" required>
            <input type="submit" value="Aceptar">
        </form>
    </div>

</body>

</html>