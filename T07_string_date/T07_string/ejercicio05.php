<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Funciones de texto - Ejercicio 5</title>
    <!-- Intercambiar un string dado, hasta volverlo a su forma original: ejemplo: Hola, ahol, laho, olah, hola (stop). -->
</head>

<body>
    <div id="content">
        <?php
        if (isset($_REQUEST['cadena'])) {
            $cadena = $_REQUEST['cadena'];
            for ($i = 0; $i <= strlen($cadena) - 1; $i++) {
                /* el último carácter lo pongo primero y le sumo el trozo siguente 
                * con substring.
                * en "hola" 'a'= 3 y le sumo "hol" = 0 1 2, y así sucesivamente */
                $cadena = substr($cadena, -1) . substr($cadena, 0, strlen($cadena) - 1);
                if ($i < strlen($cadena) - 1) {
                    echo $cadena, "<br>";
                } else {
                    echo $cadena, " (stop)"; // cuando llegue al final me muestra el mensaje
                }
            }
            echo "<br><a href='ejercicio05.php'>>>VOLVER</a>";
        } else {
        ?>
            <label for="formulario">Introduce una frase o palabra:</label>
            <form name="formulario" action="ejercicio05.php" method="post">
                <input type="text" name="cadena" required>
                <input type="submit" value="ENVIAR">
            </form>

        <?php
        } // else
        ?>

</body>

</html>