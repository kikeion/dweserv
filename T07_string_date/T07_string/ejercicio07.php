<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Funciones de texto - Ejercicio 7</title>
    <!-- Verificar si en una frase se encuentra una determinada palabra pedida al usuario. -->
</head>

<body>
    <div id="content">
        <?php
        if (isset($_REQUEST['palabra'])) {
            $palabra = $_REQUEST['palabra'];
            $texto = $_REQUEST['texto'];
            if (!mb_stripos($texto, $palabra)) {
                echo "<p>La palabra ", $palabra, " no se encuentra en el texto.</p>";
            } else {
                echo "<p>", $texto, "</p>";
                echo "<p>La palabra ", $palabra, " se encuentra en el texto en la posición ", mb_stripos($texto, $palabra), ".</p>";
            }

            echo "<br><a href='ejercicio07.php'>>>VOLVER</a>";
        } else {
        ?>
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Non itaque neque, corporis accusantium
                excepturi voluptatibus suscipit sit perferendis et inventore explicabo deserunt ut eos commodi adipisci
                iure consequuntur illum officia!</p>
            <label for="formulario">Introduce una palabra:</label>
            <form name="formulario" action="ejercicio07.php" method="post">
                <input type="text" name="palabra" required>
                <input type="hidden" name="texto" value="Lorem ipsum, dolor sit amet consectetur adipisicing elit. Non itaque neque, corporis accusantium
                excepturi voluptatibus suscipit sit perferendis et inventore explicabo deserunt ut eos commodi adipisci
                iure consequuntur illum officia!">
                <input type="submit" value="ENVIAR">
            </form>

        <?php
        } // else
        ?>

</body>

</html>