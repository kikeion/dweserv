<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Funciones de texto - Ejercicio 12</title>
    <!-- Escribir un programa que dado un texto de un telegrama que termina en punto:  
        a. contar la cantidad de palabras que posean más de 10 letras  
        b. reportar la cantidad de veces que aparece cada vocal  
        c. reportar el porcentaje de espacios en blanco.    -->
</head>

<body>
    <div id="content">
        <?php
        if (isset($_REQUEST['texto'])) {
            $texto = ((trim($_REQUEST['texto'])));
            // pongo un punto al final si no lo tuviese
            if ($texto[strlen($texto) - 1] != ".") {
                $texto = $texto . ".";
            }
            // elimino tildes:
            $arrayTexto = str_split(trim(strtolower($texto)));
            foreach ($arrayTexto as $caracter) {
                if ($caracter == 'á') {
                    $caracter = 'a';
                } else if ($caracter == 'é') {
                    $caracter = 'e';
                } else if ($caracter == 'í') {
                    $caracter = 'i';
                } else if ($caracter == 'ó') {
                    $caracter = 'o';
                } else if ($caracter == 'ú') {
                    $caracter = 'u';
                }
            }
            print_r($arrayTexto);
            echo $texto, "<br>";
            // cantidad de veces que aparece cada vocal y %espacios:
            $arrayVocales = ["a" => 0, "e" => 0, "i" => 0, "o" => 0, "u" => 0];
            $numEspacios = 0;
            for ($i = 0; $i < count($arrayTexto); $i++) {
                if (array_key_exists(($arrayTexto[$i]), $arrayVocales)) {
                    $arrayVocales[$arrayTexto[$i]]++;
                }
                if ($arrayTexto[$i] == " ") {
                    $numEspacios++;
                }
            }
            echo "<p>Cantidad de veces que aparece cada vocal:</p>";
            print_r($arrayVocales);
            echo "<p>Porcentaje de espacios en blanco:</p>";
            echo round($numEspacios * 100 / strlen($texto)), " %";
            // palabras con más de 10 letras:
            $arrayPalabras = explode(" ", $texto);
            $masDiez = 0;
            foreach ($arrayPalabras as $palabra) {
                if (strlen($palabra) > 10) {
                    $masDiez++;
                }
            }
            echo "<p>Palabras con más de diez letras: ", $masDiez, "</p>";
        }
        ?>
        <label for="formulario">Introduce un texto:</label>
        <form name="formulario" action="ejercicio12.php" method="post">
            <input type="text" name="texto" required>
            <input type="submit" value="ENVIAR">
        </form>
</body>

</html>