<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Funciones de texto - Ejercicio 1</title>
    <!-- Imprimir carácter por carácter un string dado, cada uno en una línea distinta. -->
</head>

<body>

    <form action="ejercicio01.php" method="$_POST">
        <label for="texto">Introduce un texto:</label><br>
        <textarea name="texto" id="texto" cols="30" rows="5"></textarea><br>
        <input type="submit" value="Aceptar">
    </form>

    <?php
    if (isset($_REQUEST['texto'])) {
        $cadena = $_REQUEST['texto'];
        for ($i = 0; $i < strlen($cadena); $i++) {
            echo $cadena[$i] . "<br>";
        }
    }
    ?>

</body>

</html>