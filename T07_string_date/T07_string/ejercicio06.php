<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Funciones de texto - Ejercicio 6</title>
    <!-- Dado un párrafo con dos frases (separadas por un punto), contar cuántas palabras tiene cada frase.  -->
</head>

<body>
    <div id="content">
        <?php
        if (isset($_REQUEST['frase'])) {
            $frase = $_REQUEST['frase'];
            // elimino espacios en blanco del principio y final:
            $frase = trim($frase);
            // creo un array con dos elementos (frases) separando por el punto (si hay punto final me da igual):
            $fraseArray =  explode(".", $frase);
            echo $fraseArray[0], "<br>";
            echo $fraseArray[1], "<br>";
            // creamos un array para cada elemento separando por espacios, eliminando espacios y contamos elementos=palabras:
            $frase1 = explode(" ", trim($fraseArray[0]));
            $palabras1 = count($frase1);
            $frase2 = explode(" ", trim($fraseArray[1]));
            $palabras2 = count($frase2);

            echo "<p>La primera frase tiene ", $palabras1, " palabras</p>";
            echo "<p>La segunda frase tiene ", $palabras2, " palabras</p>";

            echo "<br><a href='ejercicio06.php'>>>VOLVER</a>";
        } else {
        ?>
            <label for="formulario">Introduce dos frases separadas por un punto.</label>
            <form name="formulario" action="ejercicio06.php" method="post">
                <input type="text" name="frase" required>
                <input type="submit" value="ENVIAR">
            </form>

        <?php
        } // else
        ?>

</body>

</html>