<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Funciones de texto - Ejercicio 13</title>
    <!-- Escribir un programa que dado un texto que finaliza en punto, se pide:  
    a. la posición inicial de la palabra más larga y su longitud 
    b. cuantas palabras con una longitud entre 8 y 16 caracteres poseen más de tres veces la vocal “a” 
    Notas: 
    1.- Las palabras pueden estar separadas por uno o más espacios en blanco. 
    2.- Pueden haber varios espacios en blanco antes de la primera palabra y también después de la última. 
    3.- Se considera que una palabra finaliza cuando se encuentra un espacio en blanco o un signo de puntuación.     -->
</head>

<body>
    <div id="content">
        <?php
        if (isset($_REQUEST['texto'])) {
            $texto = ((trim($_REQUEST['texto'])));
            // pongo el punto al final si no lo tuviese
            if ($texto[strlen($texto) - 1] != ".") {
                $texto = $texto . ".";
            }
            echo $texto, "<br>";
            $arraySignos = [",", ".", ";", ":"]; // array de signos para eliminarlos
            for ($i = 0; $i < strlen($texto); $i++) {
                if (in_array($texto[$i], $arraySignos)) {
                    $texto[$i] = " ";
                }
            }
            $arrayPalabras = explode(" ", trim($texto));
            $masLarga = "";
            $palabraOcho = "";
            $cuentaA = 0;
            $palabraOchotres = 0;
            foreach ($arrayPalabras as $palabra) {
                if (strlen($palabra) >= strlen($masLarga)) {
                    $masLarga = $palabra;
                }
                if (strlen($palabra) >= 8 && strlen($palabra) <= 16) {
                    $palabraOcho = $palabra;
                }
            }
            for ($i = 0; $i < strlen($palabraOcho); $i++) {
                if (strtolower($palabraOcho[$i]) == 'a') {
                    $cuentaA++;
                }
            }
            if ($cuentaA > 3) {
                $palabraOchotres++;
            }
            echo "<p>La palabra más larga es <b>", $masLarga, "</b> y tiene una longitud de ", strlen($masLarga), " caracteres.</p>";
            echo "<p>Hay ", $palabraOchotres, " palabras entre 8 y 16 caracteres con más de 3 'a'</p>";
        }
        ?>
        <label for="formulario">Introduce un texto:</label>
        <form name="formulario" action="ejercicio13.php" method="post">
            <input type="text" name="texto" required>
            <input type="submit" value="ENVIAR">
        </form>
</body>

</html>