<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Fechas - Ejercicio 19</title>
    <!-- Mostrar el día de la semana una vez transcurridos un numero de años, meses, y días, 
        elegidos por el usuario, desde la fecha actual. -->
</head>

<body>
    <div id="content">
        <?php
        if (isset($_REQUEST['dias'])) {
            $dias = strtolower($_REQUEST['dias']);
            $meses = strtolower($_REQUEST['meses']);
            $anios = strtolower($_REQUEST['anios']);
            $fecha = date("d-m-Y"); // fecha actual
            // sumamos
            $fechaFutura = strtotime($fecha . "+ $dias days + $meses month + $anios year");
            $fecha = date("d/m/Y", $fechaFutura);
            print_r(date("d / M / Y", $fechaFutura));
            $arraySemana = [
                0 => 'domingo',
                1 => 'lunes',
                2 => 'martes',
                3 => 'miércoles',
                4 => 'jueves',
                5 => 'viernes',
                6 => 'sábado',
            ];
            $diaSemana = date("w", $fechaFutura);
            echo "<p>El día de la semana dentro de " . $dias . " días, " . $meses . " meses y " . $anios . " años es <b>" . strtoupper($arraySemana[$diaSemana]) . "</b></p>";
        }
        ?>
        <label for="formulario">Introduce número de días, meses y años:</label>
        <form name="formulario" action="#" method="POST">
            <label for="dias">Días</label>
            <input type="number" name="dias" min='0' required>
            <label for="meses">Meses</label>
            <input type="number" name="meses" min='0' required>
            <label for="anios">Años:</label>
            <input type="number" name="anios" min='0' required>
            <input type="submit" value="ENVIAR">
        </form>
</body>

</html>