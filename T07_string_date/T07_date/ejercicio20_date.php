<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Fechas - Ejercicio 20</title>
    <!-- Pedir fecha de nacimiento y una fecha futura, y mostrar la edad que 
    tendrá en esa fecha (tener en cuenta que un año tiene 60*60*24*365.25 segundos). -->
</head>

<body>
    <div id="content">
        <?php
        if (isset($_REQUEST['fnacim'])) {
            $fnacim = strtotime($_REQUEST['fnacim']);
            echo "<p>Fecha Nacimiento: " . date("d/m/Y", $fnacim) . "</p>";
            $ffutura = strtotime($_REQUEST['ffutura']);
            $anios = (int)(($ffutura - $fnacim) / (60 * 60 * 24 * 365.25));
            echo "<p>El " . date("d/m/Y", $ffutura) . " tendrás " . date($anios) . " años.</p>";
        }
        ?>
        <label for="formulario">Introduce tu fecha de nacimiento y una fecha futura:</label>
        <form name="formulario" action="#" method="POST">
            <input type="date" name="fnacim" id="fnacim" required>
            <input type="date" name="ffutura" id="ffutura" required>
            <input type="submit" value="ENVIAR">
        </form>
</body>

</html>