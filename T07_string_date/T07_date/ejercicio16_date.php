<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Fechas - Ejercicio 16</title>
    <!-- Pedir una fecha y mostrar a que día de la semana corresponde (en español). -->
</head>

<body>
    <div id="content">
        <?php
        if (isset($_REQUEST['dia'])) {

            $dia = $_REQUEST['dia'];
            $mes = $_REQUEST['mes'];
            $anio = $_REQUEST['año'];
            $dias = array("domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado");

            if (checkdate($mes, $dia, $anio)) {
                $diaSemana = date("w", strtotime($anio . "/" . $mes . "/" . $dia));
                echo "<p>Día de la semana: ", $dias[$diaSemana], "</p>";
            } else {
                echo "<p>La fecha seleccionada no es correcta.</p>";
            }

            echo "<br><a href='ejercicio16_date.php'>>>VOLVER</a>";
        } else {
        ?>
            <label for="formulario">Introduce una fecha:</label>
            <form name="formulario" action="ejercicio16_date.php" method="get">
                <label for="dia">Día:</label>
                <input type="number" name="dia" min="1" max="31" required>
                <label for="mes">Mes:</label>
                <input type="number" name="mes" min="1" max="12" required>
                <label for="año">Año:</label>
                <input type="number" name="año" min="1920" max="2020" required>
                <input type="submit" value="ENVIAR">
            </form>

        <?php
        } // else
        ?>

</body>

</html>