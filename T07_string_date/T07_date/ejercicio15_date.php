<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Fechas - Ejercicio 15</title>
    <!-- Lo mismo que el anterior pero para la hora.  -->
</head>

<body>
    <div id="content">
        <?php
        if (isset($_REQUEST['hora']) || isset($_REQUEST['minutos']) || isset($_REQUEST['segundos'])) {
            $horaString = $_REQUEST['hora'] . $_REQUEST['minutos'] . $_REQUEST['segundos'];
            $hora = $_REQUEST['hora'];
            $min = $_REQUEST['minutos'];
            $seg = $_REQUEST['segundos'];
            $formato = $_REQUEST['formato'];

            if (($hora < 0 || $hora > 23) || ($min < 0 || $min > 59) || ($seg < 0 || $seg > 59)) {
                echo "<p>La fecha seleccionada no es correcta.</p>";
            } else {
                echo "<p>Hora: ", date($formato, strtotime("$horaString")), "</p>";
            }
        }
        ?>
        <label for="formulario">Introduce una hora:</label>
        <form name="formulario" action="ejercicio15_date.php" method="POST">
            <label for="hora">Hora:</label>
            <input type="text" name="hora">

            <label for="minuto">Minuto:</label>
            <input type="text" name="minutos">

            <label for="segundos">Segundos</label>
            <input type="text" name="segundos">

            <select name="formato" id="formato">
                <option value="H:i:00">hh/mm/ss</option>
                <option value="H:i:s">h/mm/ss</option>
                <option value="g:i a">5:16 pm</option>
                <option value="h:i:s A">23:49 pm</option>
                <option value="\L\a\s H \y i">Las 8 y 23</option>
            </select>
            <input type="submit" value="ENVIAR">
        </form>
</body>

</html>