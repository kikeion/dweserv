<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Fechas - Ejercicio 17</title>
    <!-- Pedir una fecha y mostrarla en el formato “12 de Enero de 2018” (en español). -->
</head>

<body>
    <div id="content">
        <?php
        if (isset($_REQUEST['dia'])) {

            $dia = $_REQUEST['dia'];
            $mes = $_REQUEST['mes'];
            $anio = $_REQUEST['año'];
            $arrayMeses = [
                0 => 'enero',
                1 => 'febrero',
                2 => 'marzo',
                3 => 'abril',
                4 => 'mayo',
                5 => 'junio',
                6 => 'julio',
                7 => 'agosto',
                8 => 'septiembre',
                9 => 'octubre',
                10 => 'noviembre',
                11 => 'diciembre'
            ];

            if (checkdate($mes, $dia, $anio)) {
                echo "<p>La fecha es: ", $dia, " de ", $arrayMeses[$mes - 1], " de ", date("Y", strtotime("$anio-$mes-$dia")), "</p>";
            } else {
                echo "<p>La fecha seleccionada no es correcta.</p>";
            }

            echo "<br><a href='ejercicio17_date.php'>>>VOLVER</a>";
        } else {
        ?>
            <label for="formulario">Introduce una fecha:</label>
            <form name="formulario" action="ejercicio17_date.php" method="POST">
                <label for="dia">Día:</label>
                <input type="number" name="dia" required>
                <label for="mes">Mes:</label>
                <input type="number" name="mes" required>
                <label for="año">Año:</label>
                <input type="number" name="año" required>
                <input type="submit" value="ENVIAR">
            </form>

        <?php
        } // else
        ?>

</body>

</html>