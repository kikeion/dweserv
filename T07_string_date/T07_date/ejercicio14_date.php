<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Fechas - Ejercicio 14</title>
    <!-- A través de un formulario, el usuario introduce una fecha, si no es correcta se debe indicar en un mensaje; 
    si es correcta se debe mostrar en el formato elegido, crea botones de opción para configurar todas las posibilidades 
    de los dígitos del dia, mes y año.  -->
</head>

<body>
    <div id="content">
        <?php
        if (isset($_REQUEST['dia']) || isset($_REQUEST['mes']) || isset($_REQUEST['anio'] )) {

            $dia = $_REQUEST['dia'];
            $mes = $_REQUEST['mes'];
            $anio = $_REQUEST['año'];
            $formato = $_REQUEST['formato'];

            if (checkdate($mes, $dia, $anio)) {
                $fecha = date($formato, strtotime("$anio-$mes-$dia"));
                echo "<p>Fecha: ", $fecha, "</p>";
            } else {
                echo "<p>La fecha seleccionada no es correcta.</p>";
            }
        }
        ?>
        <label for="formulario">Introduce una fecha:</label>
        <form name="formulario" action="ejercicio14_date.php" method="POST">
            <label for="dia">Día:</label>
            <input type="text" name="dia">

            <label for="mes">Mes:</label>
            <input type="text" name="mes">

            <label for="año">Año</label>
            <input type="text" name="año">

            <select name="formato" id="formato">
                <option value="d/m/y">DD - MM - AA</option>
                <option value="d/m/Y">dd/mm/aaaa</option>
                <option value="\D\í\a: d \M\e\s: m \A\ñ\o: y">Día: Mes: Año:</option>
                <option value="D d M Y">D d - M - Y</option>
                <option value="d.m.y">d.m.y</option>
            </select>
            <input type="submit" value="ENVIAR">
        </form>
</body>

</html>