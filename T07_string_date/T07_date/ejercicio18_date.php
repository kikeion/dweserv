<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Fechas - Ejercicio 18</title>
    <!-- Mostrar la fecha correspondiente al próximo día de la semana elegido por el usuario. -->
</head>

<body>
    <div id="content">
        <?php
        if (isset($_REQUEST['dia'])) {
            $dia = strtolower($_REQUEST['dia']);
            $fecha = "";
            $arrayDias = [
                'domingo' => 'Sunday',
                'lunes' => 'Monday',
                'martes' => 'Tuesday',
                'miércoles' => 'Wednesday',
                'jueves' => 'Thursday',
                'viernes' => 'Friday',
                'sábado' => 'Saturday',
            ];
            if (array_key_exists($dia, $arrayDias)) {
                $fecha = strtotime("Next ". $arrayDias[$dia]);
                echo "<p>El próximo ", $dia, " es el ",  date("d - m - y", $fecha),"</p>";
            } else {
                echo "<p>El día introducido no es correcto.</p>";
            }
        }
        ?>
        <label for="formulario">Introduce un día de la semana (lunes, martes...):</label>
        <form name="formulario" action="ejercicio18_date.php" method="get">
            <label for="dia">Día:</label>
            <input type="text" name="dia" required>
            <input type="submit" value="ENVIAR">
        </form>
</body>

</html>