<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 7: Fechas - Ejercicio 21</title>
    <!-- Pedir la fecha de nacimiento y el nombre de dos personas y mostrar el nombre de la mayor.  -->
</head>

<body>
    <div id="content">
        <?php
        if (isset($_REQUEST['persona1']) || isset($_REQUEST['persona2'])) {
            $fechaActual = strtotime(date("d-m-Y", time()));

            $fpersona1 = strtotime($_REQUEST['persona1']);
            $nombre1 = $_REQUEST['nombre1'];
            $edad1 = (int)(($fechaActual - $fpersona1) / (60 * 60 * 24 * 365.25));
            $fpersona2 = strtotime($_REQUEST['persona2']);
            $nombre2 = $_REQUEST['nombre2'];
            $edad2 = (int)(($fechaActual - $fpersona2) / (60 * 60 * 24 * 365.25));

            echo "<p>" . $nombre1 . " tiene " . $edad1 . " años.</p>";
            echo "<p>" . $nombre2 . " tiene " . $edad2 . " años.</p>";
            if ($edad1 > $edad2) {
                echo "<p>" . $nombre1 . " es mayor que " . $nombre2 . ".</p>";
            } else {
                echo "<p>" . $nombre2 . " es mayor que " . $nombre1 . ".</p>";
            }
            echo "<hr>";
        }
        ?>
        <label for="formulario">Introduce la fecha de nacimiento y el nombre de dos personas:</label>
        <form name="formulario" action="#" method="POST">
            <p>Fecha nacimiento y nombre de la persona 1:</p>
            <input type="date" name="persona1" id="persona2" required>
            <label for="nombre1">Nombre:</label>
            <input type="text" name="nombre1" id="nombre1" required>
            <br>
            <p>Fecha nacimiento y nombre de la persona 2:</p>
            <input type="date" name="persona2" id="persona2" required>
            <label for="nombre2">Nombre:</label>
            <input type="text" name="nombre2" id="nombre2" required><br><br>
            <input type="submit" value="ENVIAR">
        </form>
</body>

</html>