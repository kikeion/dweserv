<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Extras Tema 2 - Ejercicio 2: resultado</title>
    <!-- Diseñar una web para jugar a la lotería primitiva. En un formulario se pedirá introducir la
    combinación de 6 números entre 1 y 49 y el numero de serie entre 1 y 999. Mostrar la
    combinación generada y la introducida por el usuario en dos filas de una tabla, para que
    compruebe los aciertos. -->
</head>

<body>

    <?php
    echo "<span id='title'><h2>Resultado Lotería Primitiva</h2></span>";
    echo "<table border='solid' cellpadding='5'>";
    echo "
    <tr>
    <td colspan='7'></td>
    <td>R</td>
    <td>C</td>
    </tr>
    <tr>
    <td>Combinación ganadora</td>
    <td>", random_int(1, 49), "</td>
    <td>", random_int(1, 49), "</td>
    <td>", random_int(1, 49), "</td>
    <td>", random_int(1, 49), "</td>
    <td>", random_int(1, 49), "</td>
    <td>", random_int(1, 49), "</td>
    <td id='reintegro'>", random_int(1, 49), "</td>
    <td id='complementario'>", random_int(1, 49), "</td></tr>
    <tr><td id'espacio' colspan='7'></td></tr>
    <tr>
    <td>Tus números</td>
    <td>", $_REQUEST['1'], "</td>
    <td>", $_REQUEST['2'], "</td>
    <td>", $_REQUEST['3'], "</td>
    <td>", $_REQUEST['4'], "</td>
    <td>", $_REQUEST['5'], "</td>
    <td>", $_REQUEST['6'], "</td>
    <td>", $_REQUEST['comp'], "</td>
    <td>", $_REQUEST['reint'], "</td>
    </tr>
    </tr>
    </table>";
    ?>

    <br>
    <br>
    <button type="button" onclick="location.href='ejercicio02.html'">VOLVER A JUGAR</button> <!-- botón volver -->
    <hr>
    <br>
    <h5>Código fuente:</h5>

    <?php
    highlight_string('
    <?php
    echo "<span id="title"><h2>Resultado Lotería Primitiva</h2></span>";
    echo "<table border="solid" cellpadding="5">";
    echo "<tr><td colspan="7"></td><td>R</td><td>C</td></tr>
    <tr><td>Combinación ganadora</td>
    <td>", random_int(1, 49), "</td>
    <td>", random_int(1, 49), "</td>
    <td>", random_int(1, 49), "</td>
    <td>", random_int(1, 49), "</td>
    <td>", random_int(1, 49), "</td>
    <td>", random_int(1, 49), "</td>
    <td id="reintegro">", random_int(1, 49), "</td>
    <td id="complementario">", random_int(1,49), "</td></tr>
    <tr><td id="espacio" colspan="7"></td></tr>
    <tr><td>Tus números</td>
    <td>", $_REQUEST["1"], "</td>
    <td>", $_REQUEST["2"], "</td>
    <td>", $_REQUEST["3"], "</td>
    <td>", $_REQUEST["4"], "</td>
    <td>", $_REQUEST["5"], "</td>
    <td>", $_REQUEST["6"], "</td>
    <td>", $_REQUEST["comp"], "</td>
    <td>", $_REQUEST["reint"], "</td>
    </tr></tr></table>";
    ?>');
    ?>
    
<br><br>
    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>