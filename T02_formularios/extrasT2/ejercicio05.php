<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Extras Tema 2 - Ejercicio 5: resultado</title>
    <!-- Diseñar un desarrollo web simple con PHP que dé respuesta a la necesidad que se plantea a
        continuación:
        Un operario de una fábrica recibe cada cierto tiempo un depósito cilíndrico de dimensiones
        variables, que debe llenar de aceite a través de una toma con cierto caudal disponible. Se
        desea crear una aplicación web que le indique cuánto tiempo transcurrirá hasta el llenado del
        depósito. Para calcular dicho tiempo el usuario introducirá los siguientes datos: diámetro y
        altura del cilindro y caudal de aceite (litros por minuto). Una vez introducidos se mostrará el
        tiempo total en horas y minutos que se tardará en llenar el cilindro.  -->
</head>

<body>
    <div id=content>
        <?php
        $radio = $_REQUEST["diametro"] / 2;
        $altura = $_REQUEST["altura"];
        $volumen = $volumen = M_PI * $radio * $radio *$altura;
        $caudal = $_REQUEST["caudal"]; // en litros/minuto
        $tminutos = $volumen * 1000 / $caudal; // tiempo de llenado en minutos
        $horas = intval($tminutos / 60); // tiempo de llenado en horas
        $minutos = $tminutos % 60; // minutos restantes
        echo "<table border='solid' cellpadding='5'>
                    <tr><th>VOLUMEN DEPÓSITO</th><th>CAUDAL (L/min)</th><th>TIEMPO DE LLENADO</th></tr>
                    <tr><td>", round($volumen, 2), "m<sup>3</sup></td><td>", $caudal, "</td><td>", $horas, " horas ", $minutos, " minutos</td></tr>
                </table>";
        ?>
        <br>
        <br>
        <button type="button" onclick="location.href='ejercicio05.html'">VOLVER</button> <!-- botón volver -->
        <p>Código fuente:</p>
        <div id="sourcecode">
            <?php
            highlight_string('
            <?php
            $radio = $_REQUEST["diametro"] / 2;
            $altura = $_REQUEST["altura"];
            $volumen = $volumen = M_PI * $radio * $radio *$altura;
            $caudal = $_REQUEST["caudal"]; // en litros/minuto
            $tminutos = $volumen * 1000 / $caudal; // tiempo de llenado en minutos
            $horas = intval($tminutos / 60); // tiempo de llenado en horas
            $minutos = $tminutos % 60; // minutos restantes
            echo "<table border="solid" cellpadding="5">
                        <tr><th>VOLUMEN DEPÓSITO</th><th>CAUDAL (L/min)</th><th>TIEMPO DE LLENADO</th></tr>
                        <tr><td>", round($volumen, 2), "m<sup>3</sup></td><td>", $caudal, "</td><td>", $horas, " horas ", $minutos, " minutos</td></tr>
                    </table>";
            ?>
        ');
            ?>
        </div>
    </div>
    <br><br>
    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>