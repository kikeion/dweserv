<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Extras Tema 2 - Ejercicio 4: resultado</title>
    <!-- Diseñar un desarrollo web simple con php que pida al usuario el precio de un producto en tres
        establecimientos distintos denominados “Tienda 1”, “Tienda 2” y “Tienda 3”. Una vez se
        introduzca esta información se debe calcular y mostrar el precio medio del producto en las tres
        tiendas. Mostrar en la página resultado, una tabla con un título, el precio en cada una de las
        tiendas, la media de los tres precios y la diferencia del precio de cada tienda con la media.
        Combina celdas para que quede visualmente agradable.  -->
</head>

<body>
    <div id=content>
        <?php
        $precio1 = $_REQUEST['tienda1'];
        $precio2 = $_REQUEST['tienda2'];
        $precio3 = $_REQUEST['tienda3'];
        $promedio = ($precio1 + $precio2 + $precio3) / 3;
        echo "<table border='solid' cellpadding='5'>
                    <tr><th>TIENDA</th><th>PRECIO ARTÍCULO</th><th>MEDIA</th><th>DIFERENCIA</th></tr>
                    <tr><td>TIENDA 1</td><td>", $precio1, "</td><td>", round($promedio, 2), "</td><td>", ($precio1 - $promedio), "</td></tr>
                    <tr><td>TIENDA 2</td><td>", $precio2, "</td><td>", round($promedio, 2), "</td><td>", ($precio2 - $promedio), "</td></tr>
                    <tr><td>TIENDA 3</td><td>", $precio3, "</td><td>", round($promedio, 2), "</td><td>", ($precio3 - $promedio), "</td></tr>
                </table>";
        ?>
        <br>
        <br>
        <button type="button" onclick="location.href='ejercicio04.html'">VOLVER</button> <!-- botón volver -->
        <p>Código fuente:</p>
        <div id="sourcecode">
            <?php
            highlight_string('
            <?php
            $precio1 = $_REQUEST["tienda1"];
            $precio2 = $_REQUEST["tienda2"];
            $precio3 = $_REQUEST["tienda3"];
            $promedio = ($precio1 + $precio2 + $precio3) / 3;
            echo "<table border="solid" cellpadding="5">
                        <tr><th>TIENDA</th><th>PRECIO ARTÍCULO</th><th>MEDIA</th><th>DIFERENCIA</th></tr>
                        <tr><td>TIENDA 1</td><td>", $precio1, "</td><td>", round($promedio, 2), "</td><td>", ($precio1 - $promedio), "</td></tr>
                        <tr><td>TIENDA 2</td><td>", $precio2, "</td><td>", round($promedio, 2), "</td><td>", ($precio2 - $promedio), "</td></tr>
                        <tr><td>TIENDA 3</td><td>", $precio3, "</td><td>", round($promedio, 2), "</td><td>", ($precio3 - $promedio), "</td></tr>
                    </table>";
            ?>
        ');
            ?>
        </div>
    </div>
    <br><br>
    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>