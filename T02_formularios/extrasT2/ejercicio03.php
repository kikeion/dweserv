<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <!-- <link href="default.css" rel="stylesheet" type="text/css" /> -->
    <title>Extras Tema 2 - Ejercicio 3: resultado</title>
    <!-- Mostrar en una página varios parametros para configurar el aspecto de otra página: color de
        fondo, tipo de letra, alineación del texto, imagen del banner (entre 3 posibles), y demás reglas
        de estilo que se te ocurran. Una vez la información se mostrará una página con el contenido
        que desees acorde a los estilos elegidos. -->
</head>

<body>
    <div id=content>
        <?php
        // COLOR DE FONDO
        $fondo = $_REQUEST['color'];
        // TIPO DE LETRA
        $letra = $_REQUEST['letra'];
        // ALINEADO TEXTO
        $alineado = $_REQUEST['alineado'];
        // IMAGEN BANNER
        $imagen = $_REQUEST['imagen'];
        echo "<body style='background-color:", $fondo, "'>";
        echo "<img src = '", $imagen, ".png' width='350' alt= 'imagen'>";
        echo "<p style='text-align:", $alineado, "; font-family:", $letra, ";' >
            Esperamos contar con la presencia de los máximos representantes de las instituciones públicas. El efecto invernadero, casi seguro, factura más de 25 millones 
            de euros al año. El branding provoca el deshielo de los polos, como consta en acta. Una mujer, especialmente una mujer negra, se convierte al crudiveganismo porque 
            tiene menos impacto en el medio ambiente. El comercio online está en el recreo, tomando café y un pitufo de tortilla. Al finalizar la sesión de mentoring mediante una call, 
            los founders estarán preparados para hacer el haunting de los developers. En este tipo de inversiones se pueden dar ganancias muy elevadas con tasas de retorno superiores al 
            1000% en algunos casos.</p>";
        ?>
        <br>
        <br>
        <button type="button" onclick="location.href='ejercicio03.html'">VOLVER</button> <!-- botón volver -->
        <p>Código fuente:</p>
        <div id="sourcecode">
            <?php
            highlight_string('
        <?php
        // COLOR DE FONDO
        $fondo = $_REQUEST["color"];
        // TIPO DE LETRA
        $letra = $_REQUEST["letra"];
        // ALINEADO TEXTO
        $alineado = $_REQUEST["alineado"];
        // IMAGEN BANNER
        $imagen = $_REQUEST["imagen"];
        echo "<img src = "", $imagen, ".png" width="350" alt= "imagen">";
        echo "<p style="text-align:", $alineado, ";">
            Esperamos contar con la presencia de los máximos representantes de las instituciones públicas. El efecto invernadero, casi seguro, factura más de 25 millones 
            de euros al año. El branding provoca el deshielo de los polos, como consta en acta. Una mujer, especialmente una mujer negra, se convierte al crudiveganismo porque 
            tiene menos impacto en el medio ambiente. El comercio online está en el recreo, tomando café y un pitufo de tortilla. Al finalizar la sesión de mentoring mediante una call, 
            los founders estarán preparados para hacer el haunting de los developers. En este tipo de inversiones se pueden dar ganancias muy elevadas con tasas de retorno superiores al 
            1000% en algunos casos.</p>";
        ?>');
            ?>
        </div>
    </div>
    <br><br>
    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>