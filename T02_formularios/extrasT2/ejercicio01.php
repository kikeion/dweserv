<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Extras Tema 2 - Ejercicio 1: resultado</title>
    <!--Diseñar un formulario web que pida la altura y el diámetro de un cilindro. Una vez el usuario
        introduzca los datos y pulse el botón calcular, deberá calcularse el volumen del cilindro y
        mostrarse el resultado en el navegador. Mostrar la imagen de un cilindro junto al resultado y
        un título "Calculo del volúmen de un cilindro", intenta dar un aspecto agradable a la página de
        resultado. -->
</head>

<body>

    <?php
    $altura = $_REQUEST["altura"];
    $radio = $_REQUEST["diametro"] / 2;
    $volumen = M_PI * $radio * $radio *$altura;
    echo "<span id='title'><h2>Cálculo del volúmen de un cilindro</h2></span>";
    echo "<img width='128' height='128' src='cilindro.png' alt='cilindro'><br>";
    echo "<p>Volumen del cilindro = ", round($volumen, 2), " cm<sup>3</sup></p>";
    ?>

    <br>
    <br>
    <button type="button" onclick="location.href='ejercicio01.html'">VOLVER</button> <!-- botón volver -->
    <hr>
    <br>
    <h5>Código fuente:</h5>

    <?php
    highlight_string('
    <?php
    $altura = $_REQUEST["altura"];
    $radio = $_REQUEST["diametro"] / 2;
    $volumen = M_PI * $radio * $radio *$altura;
    echo "<span id="title"><h2>Cálculo del volúmen de un cilindro</h2></span>";
    echo "<img width="128" height="128" src="cilindro.png" alt="cilindro"><br>";
    echo "<p>Volumen del cilindro = ", round($volumen, 2), " cm<sup>3</sup></p>";
    ?>
    ');
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>