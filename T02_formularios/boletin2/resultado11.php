<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 11: resultado</title>
</head>

<body>

    <?php
    echo $_POST['kbytes'], " Kb son ", round(($_POST['kbytes'] / 1024), $precision = 2), " Mb";
    ?>

    <br><br>
    <button type="button" onclick="location.href='ejercicio11.php'">VOLVER</button> <!-- botón volver -->
    <br>
    <hr>
    <br>
    <h5>Código fuente:</h5>

    <?php
    highlight_string('
    echo $_POST["kbytes"], " Kb son ", round(($_POST["kbytes"] / 1024), $precision = 2), " Mb";
        ');
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>