<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 8</title>
    <!-- Escribe un programa que calcule el salario semanal de un trabajador en base a las horas trabajadas.
    Se pagarán 12 euros por hora. -->
</head>

<body>

    <h2>Cálculo del salario semanal</h2>

    <form action="resultado08.php" method="post">
        Por favor, introduzca las horas trabajadas:<br>
        <!-- solo números positivos 'min=0' y de 1 en 1; 'autofocus': fija el foco en ese campo -->
        Horas <input type="number" name="horas" step="1" min="0" autofocus><br>
        <input type="submit" value="Calcular">
    </form>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>