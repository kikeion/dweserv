<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 10</title>
    <!-- Realiza un conversor de Mb a Kb. -->
</head>

<body>

    <h2>Conversor de Mb a Kb</h2>

    <form action="resultado10.php" method="post">
        Por favor, introduzca la cantidad de Mb:<br>
        <input type="number" name="mbytes" min="0" autofocus>&nbsp;Mbytes<br>
        <input type="submit" value="Calcular">
    </form>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>