<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 9</title>
    <!-- Escribe un programa que calcule el volumen de un cono mediante la fórmula 
    V = 1/3Pir2*h. -->
</head>

<body>

    <h2>Cálculo del volumen de un cono</h2>

    <form action="resultado09.php" method="post">
        Por favor, introduzca la altura y el radio del cono en centímetros:<br>
        <!-- solo números positivos 'min=0' y de 1 en 1; 'autofocus': fija el foco en ese campo -->
        Altura <input type="number" name="altura" step="0.01" min="0" autofocus><br>
        Radio <input type="number" name="radio" step="0.01" min="0"><br>
        <input type="submit" value="Calcular">
    </form>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>