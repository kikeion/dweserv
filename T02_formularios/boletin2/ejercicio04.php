<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 4</title>
    <!-- Escribe un programa que sume, reste, multiplique y divida dos números introducidos por teclado. -->
</head>

<body>

    <form action="resultado04.php" method="post">
        Por favor, introduzca dos números:<br>
        número 1 <input type="number" name="a"><br>
        número 2 <input type="number" name="b"><br>
        <input type="submit" value="Aceptar">
    </form>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>