<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 6</title>
    <!-- Escribe un programa que calcule el área de un triángulo. -->
</head>

<body>

    <h2>Cálculo del área de un triángulo</h2>

    <form action="resultado06.php" method="post">
        Por favor, introduzca la base y la altura triángulo en cm:<br>
        base <input type="number" name="base" step="0.01" min="0"><br> <!-- solo números positivos 'min=0' y de 0.01 en 0.01-->
        altura <input type="number" name="altura" step="0.01" min="0"><br>
        <input type="submit" value="Calcular">
    </form>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>