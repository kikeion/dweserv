<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 2: resultado</title>
    <!--Realiza un programa que pida dos números y luego muestre el resultado de su multiplicación. -->
</head>

<body>

    <?php
    $euros = $_POST['euros'];
    $pesetas = $euros * 166.386;
    echo $euros, " euros son ", round($pesetas, $precision = 2), " pesetas "; // redondeo a 2 decimales
    ?>

    <br>
    <br>
    <button type="button" onclick="location.href='ejercicio02.php'">VOLVER</button> <!-- botón volver -->
    <hr>
    <br>
    <h5>Código fuente:</h5>

    <?php
    highlight_string('
        $euros = $_POST["euros"];
        $pesetas = $euros * 166.386;
        echo $euros, " euros son ", round($pesetas, $precision = 2), " pesetas ";
    ');
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>