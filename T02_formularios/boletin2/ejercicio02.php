<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 2</title>
    <!--Realiza un conversor de euros a pesetas. Ahora la cantidad en euros que se quiere convertir se
deberá introducir por teclado. -->
</head>

<body>

    <h2>Conversor de euros a pesetas</h2>

    <form action="resultado02.php" method="post">
        Introduzca la cantidad en euros:<br>
        euros <input type="number" name="euros"><br>
        <input type="submit" value="Convierte">
    </form>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>