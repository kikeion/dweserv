<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 11</title>
    <!-- Realiza un conversor de Kb a Mb. -->
</head>

<body>

    <h2>Conversor de Kb a Mb</h2>

    <form action="resultado11.php" method="post">
        Por favor, introduzca la cantidad de Kb:<br>
        <input type="number" name="kbytes" min="0" autofocus>&nbsp;Kbytes<br>
        <input type="submit" value="Calcular">
    </form>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>