<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 7: resultado</title>
</head>

<body>

    <?php
    echo "BASE IMPONIBLE = ", $_POST['baseImponible'], " euros<br>";
    echo "IVA (21%)<br>";
    echo "TOTAL = ", round(($_POST['baseImponible'] * 0.21) + $_POST['baseImponible'], $precision = 2), " euros";
    ?>

    <br>
    <button type="button" onclick="location.href='ejercicio07.php'">VOLVER</button> <!-- botón volver -->
    <br>
    <hr>
    <br>
    <h5>Código fuente:</h5>

    <?php
    highlight_string('
        echo "BASE IMPONIBLE = ", $_POST["baseImponible"], " euros<br>";
        echo "IVA (21%)<br>";
        echo "TOTAL = ", round(($_POST["baseImponible"] * 0.21) + $_POST["baseImponible"], $precision = 2), " euros";
        ');
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>