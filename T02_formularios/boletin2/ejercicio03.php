<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 3</title>
    <!-- Realiza un conversor de pesetas a euros. La cantidad en pesetas que se quiere convertir se deberá
introducir por teclado. -->
</head>

<body>

    <h2>Conversor de pesetas a euros</h2>

    <form action="resultado03.php" method="post">
        Introduzca la cantidad en pesetas:<br>
        euros <input type="number" name="pesetas"><br>
        <input type="submit" value="Convierte">
    </form>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>