<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 9: resultado</title>
</head>

<body>

    <?php
    echo "VOLUMEN = ", round((pi() * $_POST['radio'] * $_POST['radio'] * $_POST['altura']) / 3, $precision = 2), " cm<sup>3</sup><br>";
    ?>

    <br><br>
    <button type="button" onclick="location.href='ejercicio09.php'">VOLVER</button> <!-- botón volver -->
    <br>
    <hr>
    <br>
    <h5>Código fuente:</h5>

    <?php
    highlight_string('
    echo "VOLUMEN = ", (pi() * $_POST["radio"] * $_POST["radio"] * $_POST["altura"]) / 3, " cm<sup>3</sup><br>";
        ');
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>