<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 1: resultado</title>
    <!--Realiza un programa que pida dos números y luego muestre el resultado de su multiplicación. -->
</head>

<body>

    <?php
    $x = $_POST['x'];
    $y = $_POST['y'];
    echo $x, " * ", $y, " = ", $x * $y;
    ?>

    <br>
    <button type="button" onclick="location.href='ejercicio06.php'">VOLVER</button> <!-- botón volver -->
    <br>
    <hr>
    <br>
    <h5>Código fuente:</h5>
    
    <?php
    highlight_string('
        <?php
        $x = $_POST["x"];
        $y = $_POST["y"];
        echo $x, " * ", $y, " = ", $x * $y;
        ?>
    ');
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>