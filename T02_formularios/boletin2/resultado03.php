<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 3: resultado</title>
</head>

<body>

    <?php
    $pesetas = $_POST['pesetas'];
    $euros = $pesetas / 166.386;
    echo $pesetas, " pesetas son ", round($euros, $precision = 2), " euros "; // redondeo a 2 decimales
    ?>

    <br>
    <br>
    <button type="button" onclick="location.href='ejercicio03.php'">VOLVER</button> <!-- botón volver -->
    <hr>
    <br>
    <h5>Código fuente:</h5>

    <?php
    highlight_string('
        $pesetas = $_POST["pesetas"];
        $euros = $pesetas / 166.386;
        echo $pesetas, " pesetas son ", round($euros, $precision = 2), " euros "; // redondeo a 2 decimales
    ');
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>