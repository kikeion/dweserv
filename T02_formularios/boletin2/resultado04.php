<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 4: resultado</title>
</head>

<body>

    <?php
    $a = $_POST['a'];
    $b = $_POST['b'];

    echo $a, " + ", $b, " = ", ($a + $b), "<br>";
    echo $a, " - ", $b, " = ", ($a - $b), "<br>";
    echo $a, " * ", $b, " = ", ($a * $b), "<br>";
    echo $a, " / ", $b, " = ", round($a / $b, $precision = 2), "<br>";

    ?>

    <br>
    <button type="button" onclick="location.href='ejercicio04.php'">VOLVER</button> <!-- botón volver -->
    <hr>
    <br>
    <h5>Código fuente:</h5>

    <?php
    highlight_string('
        $a = $_POST["a"];
        $b = $_POST["b"];
        
        echo $a, " + ", $b, " = ", ($a + $b), "<br>";
        echo $a, " - ", $b, " = ", ($a - $b), "<br>";
        echo $a, " * ", $b, " = ", ($a * $b), "<br>";
        echo $a, " / ", $b, " = ", round($a / $b, $precision = 2), "<br>";
    ');
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>