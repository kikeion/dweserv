<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>Tema 2 - Ejercicio 1</title>
  <link href="default.css" rel="stylesheet" type="text/css" />
  <!--Realiza un programa que pida dos números y luego muestre el resultado de su multiplicación. -->
</head>

<body>
  Por favor, introduce dos números: <br>
  <form action="resultado01.php" method="post">
    x <input type="number" name="x"><br>
    y <input type="number" name="y"><br>
    <input type="submit" value="Multiplica">
  </form>

  <div id="footer">
    <hr>
    <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
  </div>

</body>

</html>