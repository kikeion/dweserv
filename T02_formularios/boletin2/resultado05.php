<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 5: resultado</title>
</head>

<body>

    <?php
    $a = $_POST['a'];
    $b = $_POST['b'];
    $area = $a * $b;

    echo "Área del rectángulo = ", round($area, $precision = 2);

    ?>
    <br>
    <br>
    <button type="button" onclick="location.href='ejercicio05.php'">VOLVER</button> <!-- botón volver -->
    <hr>
    <br>
    <h5>Código fuente:</h5>

    <?php
    highlight_string('
        $a = $_POST["a"];
        $b = $_POST["b"];
        $area = $a * $b;

        echo "Área del rectángulo = ", round($area, $precision = 2);
    ');
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>