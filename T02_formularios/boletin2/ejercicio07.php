<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 7</title>
    <!-- Escribe un programa que calcule el total de una factura a partir de la base imponible. -->
</head>

<body>

    <h2>Cálculo del total de una factura</h2>

    <form action="resultado07.php" method="post">
        Por favor, introduzca la base imponible en euros:<br>
        <!-- solo números positivos 'min=0' y de 0.01 en 0.01; 'autofocus': fija el foco en ese campo -->
        Base Imponible <input type="number" name="baseImponible" step="0.01" min="0" autofocus><br>
        <input type="submit" value="Calcular">
    </form>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>