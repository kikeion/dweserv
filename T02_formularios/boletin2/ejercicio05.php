<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 5</title>
    <!-- Escribe un programa que calcule el área de un rectángulo. -->
</head>

<body>

    <h2>Cálculo del área de un rectángulo</h2>

    <form action="resultado05.php" method="post">
        Por favor, introduzca los lados del rectánculo:<br>
        ancho <input type="number" name="a" min="0"><br> <!-- solo números positivos 'min=0' -->
        alto <input type="number" name="b" min="0"><br>
        <input type="submit" value="Aceptar">
    </form>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>