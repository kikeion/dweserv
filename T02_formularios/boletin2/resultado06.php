<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 6: resultado</title>
</head>

<body>

    <?php

    echo "Área del triángulo = ", round(($_POST['base'] * $_POST['altura']) / 2, $precision = 2), " cm<sup>2</sup>";

    ?>

    <br><br>
    <button type="button" onclick="location.href='ejercicio06.php'">VOLVER</button> <!-- botón volver -->
    <hr>
    <br>
    <h5>Código fuente:</h5>

    <?php
    highlight_string('
        echo "Área del triángulo = ", round(($_POST["base"] * $_POST["altura"]) / 2, $precision = 2), " cm<sup>2</sup>";
        ');
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>