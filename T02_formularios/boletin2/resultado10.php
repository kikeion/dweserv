<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 10: resultado</title>
</head>

<body>

    <?php
    echo $_POST['mbytes'], " Mb son ", $_POST['mbytes'] * 1024, " Kb";
    ?>

    <br><br>
    <button type="button" onclick="location.href='ejercicio10.php'">VOLVER</button> <!-- botón volver -->
    <br>
    <hr>
    <br>
    <h5>Código fuente:</h5>

    <?php
    highlight_string('
    echo $_POST["mbytes"], " son ", $_POST["mbytes"] * 1024, " Kb";
        ');
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>