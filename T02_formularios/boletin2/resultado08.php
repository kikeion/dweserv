<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="default.css" rel="stylesheet" type="text/css" />
    <title>Tema 2 - Ejercicio 8: resultado</title>
</head>

<body>

    <?php
    echo "HORAS TRABAJADAS: ", $_POST['horas'], " horas<br>";
    echo "PRECIO HORA: 12 euros/hora<br>";
    echo "SALARIO SEMANAL = ", ($_POST['horas'] * 12), " euros";
    ?>

    <br><br>
    <button type="button" onclick="location.href='ejercicio08.php'">VOLVER</button> <!-- botón volver -->
    <br>
    <hr>
    <br>
    <h5>Código fuente:</h5>

    <?php
    highlight_string('
        echo "HORAS TRABAJADAS: ", $_POST["horas"], " horas<br>";
        echo "PRECIO: 12 euros/hora<br>";
        echo "SALARIO SEMANAL = ", ($_POST["horas"] * 12), " euros";
        ');
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>