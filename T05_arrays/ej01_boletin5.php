<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link href="default.css" rel="stylesheet" type="text/css">
    <title>
        Ejercicio 1 - Boletín 5
    </title>
    <!-- Define tres arrays de 20 números enteros cada una, con nombres “numero”, “cuadrado” y “cubo”.
        Carga el array “numero” con valores aleatorios entre 0 y 100. En el array “cuadrado” se deben
        almacenar los cuadrados de los valores que hay en el array “numero”. En el array “cubo” se deben
        almacenar los cubos de los valores que hay en “numero”. A continuación, muestra el contenido de
        los tres arrays dispuesto en tres columnas. -->
</head>

<body>
    <header id="titulo">
        <h2>
            Tema Arrays: Ejercicio 1
        </h2>
    </header>
    <div id="content">
        <?php
        echo "<table><tr><th>NUM</th><th>NUM<sup>2</sup></th><th>NUM<sup>3</sup></th></tr>";
        for ($i = 0; $i < 20; $i++) {
            // números aleatorios entre 0 y 100 (ambos incluidos)
            $numero[$i] = rand(0, 100);
        }
        for ($i = 0; $i < 20; $i++) {
            $cuadrado[$i] = pow($numero[$i], 2);
        }
        for ($i = 0; $i < 20; $i++) {
            $cubo[$i] = pow($numero[$i], 3);
        }
        // imprimo el contenido de la tabla:
        for ($i = 0; $i < 20; $i++) {
            echo "<tr><td>", $numero[$i], "</td><td>", $cuadrado[$i], "</td><td>", $cubo[$i], "</td></tr>";
        }
        echo "</table>";
        ?>
    </div>
    <div class="codigo_fuente">
        <br>
        <h5>
            Código fuente:
        </h5>
        <?php
        highlight_string('
        <?php
        echo "<table><tr><th>NUM</th><th>NUM<sup>2</sup></th><th>NUM<sup>3</sup></th></tr>";
        for ($i = 0; $i < 20; $i++) {
        // números aleatorios entre 0 y 100 (ambos incluidos)
        $numero[$i] = rand(0, 100);
        }            
        for ($i = 0; $i < 20; $i++) {
        $cuadrado[$i] = pow($numero[$i], 2);
        }
        for ($i = 0; $i < 20; $i++) {
        $cubo[$i] = pow($numero[$i], 3);
        }
        // imprimo el contenido de la tabla:
        for ($i = 0; $i < 20; $i++) {
            echo "<tr><td>", $numero[$i], "</td><td>", $cuadrado[$i], "</td><td>", $cubo[$i], "</td></tr>";
        }
        echo "</table>";
    ?>
        
        ');
        ?>
        <div id="footer">
            <hr>
            <p>
                © David Benítez Cabeza - 2ºDAW 2020/21
            </p>
            </hr>
        </div>