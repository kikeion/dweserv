<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Extras Tema 5 - Ejercicio 1</title>
    <link rel="stylesheet" href="style.css">
    <!-- Diseñar una página que esté compuesta por una tabla de 10 filas por 10 columnas, y en cada celda habrá una imagen de un ojo cerrado. 
    Cada vez que el usuario pulse un ojo, ser recargará la página con todos los ojos cerrados salvo los que se han ido pulsando que corresponderán 
    a un ojo abierto. Conforme se vayan pulsando más ojos se irá completando la tabla de ojos abiertos. Si se pulsa en un ojo abierto se volverá a 
    cerrar. Usar la función explode() para pasar arrays como cadenas. -->
</head>

<body>
    <div id="titulo">
        <h2>Extras Tema 5: Arrays - Ejercicio 1</h2>
    </div>

    <div id="contenido">
        <?php
        // SI LLEGA LA POSICIÓN PINCHADA COMO PARÁMETRO //
        if (isset($_REQUEST['f'])) {
            // recojo posición del ojo pinchado:
            $f = $_REQUEST['f'];
            $c = $_REQUEST['c'];
            $cadena = "";
            // recojo el estado de la matriz al llegar como texto y lo convierto a array:
            $matriz = explode(";", $_REQUEST['matriz']);

            for ($i = 0; $i < 10; $i++) {
                $matriz[$i] = explode(" ", $matriz[$i]);
            }
            //echo "<pre>";
            //print_r($matriz);
            // compruebo valor de la casilla pinchada para invertir el estado abierto/cerrado:
            if ($matriz[$f][$c] == 0) {
                $matriz[$f][$c] = 1;
            } else {
                $matriz[$f][$c] = 0;
            }
            // print_r("estado recogido en la posición ". $f.", ".$c." = ".$matriz[$f][$c]);
        } else {
            // PRIMERA VEZ QUE LLEGA //
            $cadena = ""; // inicializo el estado del array como texto
            // inicializo todos los valores de la matriz a cero:
            for ($i = 0; $i < 10; $i++) {
                for ($j = 0; $j < 10; $j++) {
                    $matriz[$i][$j] = 0;
                }
            }
        }

        // ALMACENO LOS VALORES DEL ARRAY EN UNA VARIABLE TEXTO //
        foreach ($matriz as $fila) {
            $cadena = $cadena . implode(' ', (array)$fila) . ';';
            /* separa los valores con espacio y cada fila de valores por punto y coma
               el casting (array) es para evitar warning */
        }
        // elimino el punto y coma final de la cadena:
        $cadena = substr($cadena, 0, -1);

        //<<<< PINTA LA MATRIZ DE OJOS >>>>//
        echo "<table id= 'ojos'>";
        for ($i = 0; $i < 10; $i++) {
            echo "<tr>";
            for ($j = 0; $j < 10; $j++) {
                if ($matriz[$i][$j] == 1) {
                    $imagen = "img/eye-open.png";
                } else {
                    $imagen = "img/eye-closed.png";
                }
                echo "<td><a href='ejercicio01_extrasT5.php?f=$i&c=$j&matriz=$cadena.'><img src='$imagen' width='50px'></td>";
            }
            echo "</tr>";
        }
        echo "</table>";
        ?>
    </div>

    <div id="footer">
        <hr>
        <p>
            © David Benítez Cabeza - 2ºDAW 2020/21
        </p>
        </hr>
    </div>
</body>

</html>