<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Extras Tema 5 - Ejercicio 3</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <?php
    // VIENE DEL FORMULARIO:
    if (isset($_REQUEST['submit'])) {
        $respuesta = $_REQUEST['solucion'];
        $intentos = $_REQUEST['intentos'];
        $panel = unserialize($_REQUEST['panel']);
        if (strtolower($respuesta) == "yoda" || strtolower($respuesta) == "baby yoda") {
            echo "<p><b>ENHORABUENA, HAS ACERTADO!</b></p>";
            echo "<p><b>INTENTOS RESTANTES = ", $intentos, "</b></p>";
            echo "<img id='solucion' src='img/yodababy.jpg'>";
            echo "<p><a href='ejercicio03_extrasT5.php'>Inicio</a></p>";
        } else {
            echo "<p><b>LO SIENTO, NO LO HAS ADIVINADO.</b></p>";
            if ($intentos == 0) {                
                echo "<p><b>No te quedan más oportunidades!<b></p>";
                echo "<br><b>Esta era la solución: <i>baby yoda de 'The Mandalorian'</i><b><br>";
                echo "<img id='solucion' src='img/yodababy.jpg'>";
            } else {                
                echo "<p><a href='ejercicio03_extrasT5.php?intentos=$intentos&panel=", serialize($panel),"'>SEGUIR INTENTÁNDOLO</a></p>";
            }
        }
    }
    ?>
</body>

</html>