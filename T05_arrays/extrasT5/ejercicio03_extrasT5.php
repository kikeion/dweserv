<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <title>Extras Tema 5 - Ejercicio 3</title>
    <!-- Vamos a hacer el ejercicio de adivinar la imagen oculta del tema 3 más interesante. 
    Para empezar, vamos a hacer el mosaico un poco mas grande, de 10x10. 
    Además la imagen no se va a dividir sino que formará parte del fondo de la tabla y para ocultar o 
    mostrar cada parte del mosaico,el fondo de cada celda será transparente u opaco. Cada vez quese pulse un cuadrado, 
    la parte de la imagen correspondiente a ese cuadrado se mostrará de manera definitiva, así que cada vez se irán mostrando más partes de la imagen.
    Por último, para rematar y hacerlo todavía más interesante, vamos a poner un límite en el número de cuadrados volteados, de modo que, 
    si no se adivina la imagen antes de superar ese límite, se mostrará un mensaje indicando que ha perdido junto a la imagen completa. 
    Si se acierta introduciendo el nombre correcto en la caja de texto antes de superar el límite,también se indicará con un mensaje junto a la imagen completa.
    Ayuda: La tabla con las partes visibles y no visibles de la imagen, se encuentra en una única página que se recarga con cada pulsación, 
    pero el resultado del juego tanto si ha ganadocomo si ha perdido,se puede realizar en otra página distinta. 
    Almacenar en un array la situación de cada celda (vista u oculta) y pasar el array con la función serialize para mayor comodidad. -->
    <style>
        body {
            background-color: aliceblue;
            font-family: Arial, Helvetica, sans-serif;
        }

        table,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        table {
            background-image: url(img/yodababy.jpg);
        }

        #visible {
            background-color: transparent;
        }

        #oculto {
            background-color: green;
        }

        #fieldset {
            text-align: left;
            width: 50%;
        }

        #footer {
            font-family: monospace;
            position: absolute;
            bottom: 0;
        }
    </style>

</head>

<body>
    <div id="titulo">
        <h2>Extras Tema 5: Arrays - Ejercicio 2</h2>
    </div>

    <?php
    // RECOGE INTENTOS:
    if (!isset($_REQUEST['intentos'])) {
        $intentos = 10; // primera vez que abro la página los intentos = 10
        $panel = array_fill(0, 100, 'oculto'); // relleno el array con valor 'oculto'
        echo "<p><b>Adivina la imagen</b></p>";
        echo "<p>Te quedan ", $intentos, " intentos</p>";
        echo "<table>";
        $n = 0; // controlo el número de celda
        for ($i = 0; $i < 10; $i++) {
            echo "<tr><td id='$panel[$n]'><a href='ejercicio03_extrasT5.php?num=$n&intentos=$intentos&panel=", serialize($panel), "'><div style='height:50px;width:50px'></div></a></td>";
            $n++;
            for ($j = 0; $j < 9; $j++) {
                echo "<td id='$panel[$n]'><a href='ejercicio03_extrasT5.php?num=$n&intentos=$intentos&panel=", serialize($panel), "'><div style='height:50px;width:50px'></div></a></td>";
                $n++;
            }
            echo "</tr>";
        }
        echo "</table>";
    } else {
        $panel = unserialize($_REQUEST['panel']);
        $intentos = --$_REQUEST['intentos']; // cada vez que recibe 'intentos' le resta 1
        if (isset($_REQUEST['num'])) {
            $num = $_REQUEST['num'];
            $panel[$num] = 'visible';
        }        
        if ($intentos == 0) {
            echo "<p>No te quedan más oportunidades.</p>";
            echo "<br>Esta era la solución: baby yoda de 'The Mandalorian'.<br>";
            echo "<img id='solucion' src='img/yodababy.jpg'><br>";
            echo "<a href='ejercicio03_extrasT5.php'>VOLVER</a>";
        } else {            
            echo "<p><b>Adivina la imagen</b></p>";
            echo "<p>Te quedan ", $intentos, " intentos</p>";
            echo "<table>";
            $n = 0; // controlo el número de celda
            for ($i = 0; $i < 10; $i++) {
                echo "<tr><td id='$panel[$n]'><a href='ejercicio03_extrasT5.php?num=$n&intentos=$intentos&panel=", serialize($panel), "'><div style='height:50px;width:50px'></div></a></td>";
                $n++;
                for ($j = 0; $j < 9; $j++) {
                    echo "<td id='$panel[$n]'><a href='ejercicio03_extrasT5.php?num=$n&intentos=$intentos&panel=", serialize($panel), "'><div style='height:50px;width:50px'></div></a></td>";
                    $n++;
                }
                echo "</tr>";
            }
            echo "</table>";

            //// FORMULARIO DE RESPUESTA ////
            echo "<form name='respuesta' action='ejercicio03_extrasT5_result.php' method='post'>
            <label for='respuesta'>Solución:</label>
            <input type='text' name='solucion' required>
            <input type='hidden' name='intentos' value='$intentos'>
            <input type='hidden' name='panel' value='", serialize($panel), "'>
            <input type='submit' name='submit' value='Adivinar'>
            </form>";
        }
    }
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>