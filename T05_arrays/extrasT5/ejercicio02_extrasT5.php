<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Extras Tema 5 - Ejercicio 2</title>
    <link rel="stylesheet" href="style.css">
    <!-- Realizar una página web para realizar pedidos de comida rápida. 
    Tenemos tres tipos de comida:
    Pizza: jamon, atun, bacon, peperoni
    Hamburguesa: lechuga, tomate, queso 
    Perrito caliente: lechuga, cebolla, patata
    A traves de tres formularios distintos, uno para cada tipo de comida se va añadiendo comida al pedido con sus 
    respectivos ingredientes, hasta que se pulse el botón finalizar pedido (en otro formulario), con lo que se mostrará 
    el pedido completo en una tabla, con toda la comida y las opciones de cada una.
    Usar las función serialize() y unserialize() para pasar arrays como cadenas
    Nota: con arrays de 2 dimensiones la serialización se corrompe, así que hay que usar la función en 
    combinación con otra de la siguiente forma:
        $cadenaTexto=base64_encode(serialize($array));
        $array=unserialize(base64_decode($cadenaTexto)); -->
</head>

<body>
    <div id="titulo">
        <h2>Extras Tema 5: Arrays - Ejercicio 2</h2>
    </div>

    <div id="contenido">

        <?php
        // INICIO: si no llega el parámetro 'comida' defino el array $pedido[]
        if (!isset($_REQUEST['comida'])) {
            $pedido = [];
            // si llega el parámetro 'comida' entonces tengo que meter en el array $pedido[] la cadena de texto que me llega:
        } else {
            // recojo el pedido y le voy añadiendo la comida:
            $pedido = unserialize(base64_decode($_REQUEST['pedido']));
            $pedido[] = $_REQUEST['comida'];
        }
        // FINALIZAR PEDIDO:
        if (isset($_REQUEST['fin'])) {
            // imprimo pedido:
            echo "<div id='resultado'><table id='pedidos'><tr><td><b>PEDIDOS:</b></td></tr>";
            foreach ($pedido as $comida) {
                echo  "<tr><td>" . $comida[0] . " con: <br>";
                for ($i = 1; $i < count($comida); $i++) {
                    echo $comida[$i] . "<br>";
                }
                echo "<br></td></tr>";
            }
            echo "</table></div><br><div><a href='ejercicio02_extrasT5.php'>NUEVO PEDIDO</a></div>";
        } else {
            // muestro formularios:
        ?>
            <!-- PRIMER FORMULARIO -->
            <fieldset id="fieldset">PIZZA:<br>
                <form action="ejercicio02_extrasT5.php" method="get">
                    <!-- envía el valor comida[0]=pizza de manera oculta -->
                    <input type="hidden" name="comida[]" value="Pizza">
                    <input type="checkbox" name="comida[]" value="jamon"><label>Jamón</label><br>
                    <input type="checkbox" name="comida[]" value="atun"><label>Atún</label><br>
                    <input type="checkbox" name="comida[]" value="bacon"><label>Bacon</label><br>
                    <input type="checkbox" name="comida[]" value="peperoni"><label>Peperoni</label><br>
                    <!-- envío de manera oculta el pedido, usando la función serialize para transformar en texto el pedido -->
                    <input type="hidden" name="pedido" value="<?= base64_encode(serialize($pedido)); ?>">
                    <br><input type="submit" name="enviar" value="Añadir al pedido">
                </form>
            </fieldset>
            <!-- SEGUNDO FORMULARIO -->
            <fieldset id="fieldset">HAMBURGUESA:<br>
                <form action="ejercicio02_extrasT5.php" method="get">
                    <input type="hidden" name="comida[]" value="Hamburguesa">
                    <input type="checkbox" name="comida[]" value="lechuga"><label>Lechuga</label><br>
                    <input type="checkbox" name="comida[]" value="tomate"><label>Tomate</label><br>
                    <input type="checkbox" name="comida[]" value="cebolla"><label>Cebolla</label><br>
                    <input type="hidden" name="pedido" value="<?= base64_encode(serialize($pedido)); ?>">
                    <br><input type="submit" value="Añadir al pedido">
                </form>
            </fieldset>
            <!-- TERCER FORMULARIO -->
            <fieldset id="fieldset">PERRITO CALIENTE:<br>
                <form action="ejercicio02_extrasT5.php" method="get">
                    <input type="hidden" name="comida[]" value="Perrito Caliente">
                    <label><input type="checkbox" name="comida[]" value="lechuga">Lechuga</label><br>
                    <input type="checkbox" name="comida[]" value="patata"><label>Patata</label><br>
                    <input type="checkbox" name="comida[]" value="cebolla"><label>Cebolla</label><br>
                    <input type="hidden" name="pedido" value="<?= base64_encode(serialize($pedido)); ?>">
                    <br><input type="submit" value="Añadir al pedido">
                </form>
            </fieldset>
            <!-- FORMULARIO FINAL -->
            <form action="ejercicio02_extrasT5.php" method="get">
                <input type="hidden" name='fin' value="fin">
                <input type="hidden" name="pedido" value="<?= base64_encode(serialize($pedido)); ?>">
                <input type="hidden" name="comida[]" value="">
                <br><input type="submit" value="Finalizar Pedido">
            </form>
        <?php
        }
        ?>

    </div>

    <div id="footer">
        <hr>
        <p>
            © David Benítez Cabeza - 2ºDAW 2020/21
        </p>
        </hr>
    </div>
</body>

</html>