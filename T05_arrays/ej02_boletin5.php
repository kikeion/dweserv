<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link href="default.css" rel="stylesheet" type="text/css">
    <title>
        Ejercicio 2 - Boletín 5
    </title>
    <!-- Escribe un programa que pida 10 números por teclado y que luego muestre los números introducidos
    junto con las palabras “máximo” y “mínimo” al lado del máximo y del mínimo respectivamente. -->
</head>

<body>
    <header id="titulo">
        <h2>
            Tema Arrays: Ejercicio 2
        </h2>
    </header>
    <div id="content">
        <?php
        if (isset($_POST["numIntroducido"])) {
            $num[] = $_POST["numIntroducido"];
            $contador++;
            $mayor = 0;
            for ($i =0; $i < 10; $i++) {
                if ($num[$i] > $mayor) {
                    $mayor = $num[$i];
                } else {
                    $menor =$num[$i];
                }
            }
            foreach ($num as $elemento) {
                if ($elemento == $mayor) {
                    echo $elemento, " (mayor) ";
                } else if ($elemento == $menor) {
                    echo $elemento, " (menor) ";
                } else {
                    echo $elemento, " ";
                }
            }
        }  else if ((!isset($_POST["numIntroducido"])) || ($contador > 10)) {
            ?>
        <label for="numeros">Introduce un número:</label>
        <form name="numeros" action="ej02_boletin5.php" method="post">
        <input type="number" name="numIntroducido" required autofocus>
        <input type="hidden" name="contador">
        </form>
        <?php 
        }
        ?>
    </div>
    <div class="codigo_fuente">
        <br>
        <h5>
            Código fuente:
        </h5>
        <?php
        highlight_string('
        
        ');
        ?>
        <div id="footer">
            <hr>
            <p>
                © David Benítez Cabeza - 2ºDAW 2020/21
            </p>
            </hr>
        </div>