<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <title>Ejercicio 1 - Extras Tema 3</title>
    <!-- Partiendo del Ejercicio1, amplíalo con la siguiente funcionalidad. En la pantalla principal se
        mostrará el número de intentos que lleva el usuario, que en la primera carga será de 0 intentos
        y conforme se vayan pulsando los cuadrados para descubrir la imagen que esconden se irá
        aumentando en 1. Cuando el usuario intente adivinar la palabra de la imagen, si acierta se
        mostrará el número de intentos en que lo ha conseguido, y si ha fallado mostrará el número de
        intentos que lleva hasta ese momento, y al volver a la pantalla principal seguirá aumentando el
        número de intentos que llevaba.
        Nota: para controlar el número de intentos es necesario pasarlo como parámetro cada vez que
        se navega de una página a otra, si no se pierde su valor. -->
    <link rel="stylesheet" href="default.css" type="text/css">
    <?php
    // Recoge el parámetro que envía la imagen: 
    
    if (isset($_REQUEST["trozo"])) {  
        $trozo = $_REQUEST["trozo"];        
        echo "<meta http-equiv='refresh' content='2; url=ejercicio03extras.php?intentos'>";
    }
    if (isset($_REQUEST["intentos"])) {
        $intentos = $_REQUEST["intentos"];
    }
    ?>
</head>

<body>

    <p><b>Adivina la imagen</b></p>

    <?php
    
    // Compruebo si el parámetro que viene lo hace desde el formulario o desde la imagen:

    // SI VIENE DEL FORMULARIO:

    if (isset($_POST['submit'])) {
        $respuesta = $_POST['solucion'];
        if (strtolower($respuesta) == "koala") {
            echo "<p><b>ENHORABUENA, HAS ACERTADO!</b></p>";
            echo "<p><b>INTENTOS TOTALES: ", $intentos, "</b></p>";
            echo "<img src='img/koala-durmiendo.jpg'>";
            echo "<p><a href='ejercicio03extras.php'>VOLVER</a></p>";
        } else {
            echo "<p><b>LO SIENTO, NO LO HAS ADIVINADO. SIGUE INTENTÁNDOLO</b></p>";
        }

        // SI VIENE DE LA IMAGEN:

    } else {
        
        echo "<table id='mosaico'>
        <tr><td>";
        if (isset($trozo) && $trozo == 9) {
            echo "<img src='img/9.jpg'>";
            $intentos++;
        } else {
            echo "<img src='img/cuadradoverde.png'>";
        }

    ?></td>
        <td>
            <?php
            if (isset($trozo) && $trozo == 8) {
                echo "<img src='img/8.jpg'>";
                $intentos++;
            } else {
                echo "<img src='img/cuadradoverde.png'>";
            } ?>
        </td>
        <td>
            <?php
            if (isset($trozo) && $trozo == 7) {
                echo "<img src='img/7.jpg'>";
                $intentos++;
            } else {
                echo "<img src='img/cuadradoverde.png'>";
            }
            ?>
        </td>
        </tr>
        <tr>
            <td>
                <?php
                if (isset($trozo) && $trozo == 6) {
                    echo "<img src='img/6.jpg'>";
                    $intentos++;
                } else {
                    echo "<img src='img/cuadradoverde.png'>";
                }
                ?>
            </td>
            <td>
                <?php
                if (isset($trozo) && $trozo == 5) {
                    echo "<img src='img/5.jpg'>";
                    $intentos++;
                } else {
                    echo "<img src='img/cuadradoverde.png'>";
                }
                ?>
            </td>
            <td>
                <?php
                if (isset($trozo) && $trozo == 4) {
                    echo "<img src='img/4.jpg'>";
                    $intentos++;
                } else {
                    echo "<img src='img/cuadradoverde.png'>";
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                if (isset($trozo) && $trozo == 3) {
                    echo "<img src='img/3.jpg'>";
                    $intentos++;
                } else {
                    echo "<img src='img/cuadradoverde.png'>";
                }
                ?>
            </td>
            <td>
                <?php
                if (isset($trozo) && $trozo == 2) {
                    echo "<img src='img/2.jpg'>";
                    //$intentos++;
                } else {
                    echo "<img src='img/cuadradoverde.png'>";
                }
                ?>
            </td>
            <td>
                <?php
                if (isset($trozo) && $trozo == 1) {
                    echo "<img src='img/1.jpg'>";
                    $intentos++;
                } else {
                    echo "<img src='img/cuadradoverde.png'>";
                }
                ?>
            </td>
        </tr>
        </table>
    <?php } ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>



</body>

</html>