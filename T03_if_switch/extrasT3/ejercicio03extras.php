<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <title>Ejercicio 3 - Extras Tema 3</title>
    <!-- Partiendo del Ejercicio1, amplíalo con la siguiente funcionalidad. En la pantalla principal se
        mostrará el número de intentos que lleva el usuario, que en la primera carga será de 0 intentos
        y conforme se vayan pulsando los cuadrados para descubrir la imagen que esconden se irá
        aumentando en 1. Cuando el usuario intente adivinar la palabra de la imagen, si acierta se
        mostrará el número de intentos en que lo ha conseguido, y si ha fallado mostrará el número de
        intentos que lleva hasta ese momento, y al volver a la pantalla principal seguirá aumentando el
        número de intentos que llevaba.
        Nota: para controlar el número de intentos es necesario pasarlo como parámetro cada vez que
        se navega de una página a otra, si no se pierde su valor. -->
    <link rel="stylesheet" href="default.css" type="text/css">

</head>

<body>

    <?php 
    if (isset($_REQUEST["intentos"])) {  
        $intentos = $intentos + 1;
    } else {
        $intentos = 0;
    } ?>

    <p><b>Adivina la imagen</b></p>
    <table id="mosaico">
        <tr>
            <td><a href="ejercicio03extras_1.php?trozo=9&intentos"><img src="img/cuadradoverde.png"></a></td>
            <td><a href="ejercicio03extras_1.php?trozo=8&intentos"><img src="img/cuadradoverde.png"></a></td>
            <td><a href="ejercicio03extras_1.php?trozo=7&intentos"><img src="img/cuadradoverde.png"></a></td>
        </tr>
        <tr>
            <td><a href="ejercicio03extras_1.php?trozo=6&intentos"><img src="img/cuadradoverde.png"></a></td>
            <td><a href="ejercicio03extras_1.php?trozo=5&intentos"><img src="img/cuadradoverde.png"></a></td>
            <td><a href="ejercicio03extras_1.php?trozo=4&intentos"><img src="img/cuadradoverde.png"></a></td>
        </tr>
        <tr>
            <td><a href="ejercicio03extras_1.php?trozo=3&intentos=1"><img src="img/cuadradoverde.png"></a></td>
            <td><a href="ejercicio03extras_1.php?trozo=2&intentos"><img src="img/cuadradoverde.png"></a></td>
            <td><a href="ejercicio03extras_1.php?trozo=1&intentos"><img src="img/cuadradoverde.png"></a></td>
        </tr>
    </table>

    <form name="respuesta" action="ejercicio03extras_1.php" method="post">
        <label for="respuesta">Solución:</label>
        <input type="text" name="solucion" autofocus required>
        <input type="hidden" name="intentos">
        <input type="submit" name="submit" value="Adivinar">
    </form>   
    <?php 
        echo "<span>Llevas ", $intentos, " intentos</span>";
    ?> 

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>