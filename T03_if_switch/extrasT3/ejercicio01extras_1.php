<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <title>Ejercicio 1 - Extras Tema 3</title>
    <!-- Vamos a diseñar un juego para adivinar imágenes mostrando solo alguna parte de ellas. Dividir
        una imagen en un mosaico de 3x3, y mostrar una cuadrícula en la página principal con todos
        los cuadrados del mosaico dados la vuelta. Debajo de la cuadrícula habrá una caja de texto
        para que el usuario intente adivinar el nombre de lo que aparece en la imagen, junto a un
        botón comprobar.
        Cada vez que el usuario pulse en un cuadrado de la cuadrícula se mostrará el contenido solo de
        esa cuadrícula durante 2 segundos y posteriormente se volverá a ocultar.
        Cuando el usuario escriba algo y pulse el botón comprobar ocurrirá lo siguiente:
        -Si ha acertado se mostrará la imagen completa y un mensaje de felicitación por acertar.
        -Si no ha acertado se mostrará un mensaje indicando que ha fallado y un botón de volver para
        seguir intentándolo. -->
    <link rel="stylesheet" href="default.css" type="text/css">
    <?php
    // Recoge el parámetro que envía la imagen:
    
    if (isset($_REQUEST["trozo"])) { 
        $trozo = $_REQUEST["trozo"];
        echo "<meta http-equiv='refresh' content='2; url=ejercicio01extras.html'>";
    }
    ?>
</head>

<body>

    <p><b>Adivina la imagen</b></p>

    <?php
    
    // Compruebo si el parámetro que viene lo hace desde el formulario o desde la imagen:

    // SI VIENE DEL FORMULARIO:

    if (isset($_POST['submit'])) {
        $respuesta = $_POST['solucion'];
        if (strtolower($respuesta) == "koala") {
            echo "<p><b>ENHORABUENA, HAS ACERTADO!</b></p>";
            echo "<img src='img/koala-durmiendo.jpg'>";
            echo "<p><a href='ejercicio01extras.html'>VOLVER</a></p>";
        } else {
            echo "<p><b>LO SIENTO, NO LO HAS ADIVINADO. SIGUE INTENTÁNDOLO</b></p>";
        }

        // SI VIENE DE LA IMAGEN:

    } else {
        echo "<table id='mosaico'>
        <tr><td>";
        if (isset($trozo) && $trozo == 9) {
            echo "<img src='img/9.jpg'>";
        } else {
            echo "<img src='img/cuadradoverde.png'>";
        }

    ?></td>
        <td>
            <?php
            if (isset($trozo) && $trozo == 8) {
                echo "<img src='img/8.jpg'>";
            } else {
                echo "<img src='img/cuadradoverde.png'>";
            } ?>
        </td>
        <td>
            <?php
            if (isset($trozo) && $trozo == 7) {
                echo "<img src='img/7.jpg'>";
            } else {
                echo "<img src='img/cuadradoverde.png'>";
            }
            ?>
        </td>
        </tr>
        <tr>
            <td>
                <?php
                if (isset($trozo) && $trozo == 6) {
                    echo "<img src='img/6.jpg'>";
                } else {
                    echo "<img src='img/cuadradoverde.png'>";
                }
                ?>
            </td>
            <td>
                <?php
                if (isset($trozo) && $trozo == 5) {
                    echo "<img src='img/5.jpg'>";
                } else {
                    echo "<img src='img/cuadradoverde.png'>";
                }
                ?>
            </td>
            <td>
                <?php
                if (isset($trozo) && $trozo == 4) {
                    echo "<img src='img/4.jpg'>";
                } else {
                    echo "<img src='img/cuadradoverde.png'>";
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                if (isset($trozo) && $trozo == 3) {
                    echo "<img src='img/3.jpg'>";
                } else {
                    echo "<img src='img/cuadradoverde.png'>";
                }
                ?>
            </td>
            <td>
                <?php
                if (isset($trozo) && $trozo == 2) {
                    echo "<img src='img/2.jpg'>";
                } else {
                    echo "<img src='img/cuadradoverde.png'>";
                }
                ?>
            </td>
            <td>
                <?php
                if (isset($trozo) && $trozo == 1) {
                    echo "<img src='img/1.jpg'>";
                } else {
                    echo "<img src='img/cuadradoverde.png'>";
                }
                ?>
            </td>
        </tr>
        </table>
    <?php } ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>



</body>

</html>