<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <title>Resultado ejercicio 3 - Tema 3</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
</head>

<body>

    <br>
    <?php
        $dia = $_REQUEST["dia"];
        switch ($dia) {
            case "1":
                echo ("LUNES");
                break;
                case "2":
                    echo ("MARTES");
                    break;
                    case "3":
                        echo ("MIÉRCOLES");
                        break;
                        case "4":
                            echo ("JUEVES");
                            break;case "5":
                                echo ("VIERNES");
                                break;
                                case "6":
                                    echo ("SÁBADO");
                                    break;
                                    case "7":
                                        echo ("DOMINGO");
                                        break;
            default:
                echo ("VALOR INCORRECTO");
                break;
        }    
    ?>

    <br><br>
    <button type="button" onclick="location.href='ejercicio03.html'">VOLVER</button>
    <br>
    <hr>
    <br>
    <h5>Código fuente:</h5>
    <!-- MUESTRO EL CÓDIGO FUENTE CON highlight_string('código') -->
    <?php
    highlight_string('
    <?php
        $dia = $_REQUEST["dia"];
        switch ($dia) {
            case "1":
                echo ("LUNES");
                break;
                case "2":
                    echo ("MARTES");
                    break;
                    case "3":
                        echo ("MIÉRCOLES");
                        break;
                        case "4":
                            echo ("JUEVES");
                            break;case "5":
                                echo ("VIERNES");
                                break;
                                case "6":
                                    echo ("SÁBADO");
                                    break;
                                    case "7":
                                        echo ("DOMINGO");
                                        break;
            default:
                echo ("VALOR INCORRECTO");
                break;
        }    
    ?>
    ');
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>