<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3 -Ejercicio 6 Resultado</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
    <!-- Realiza un programa que calcule el tiempo que tardará en caer un objeto desde una altura h. 
        Aplica la fórmula t =√(2h/g) , siendo g = 9:81m/s2 -->
</head>

<body>

    <br>
    <!-- CONTENIDO -->
    <main>

        <?php
        $altura = $_REQUEST['altura'];
        $g = 9.81;
        echo "tiempo = ", round((sqrt((2*$altura)/$g)), 2), " segundos.";
        ?>

    </main>

    <br><br>
    <!-- BOTÓN VOLVER -->
    <button type="button" onclick="location.href='ejercicio06.html'">VOLVER</button>
    <br>
    <hr>
    <br>    
    <!-- CÓDIGO FUENTE -->
    <div class="codigo">
        <h5>Código fuente:</h5>
        <?php
        highlight_string('
        <?php
        $altura = $_REQUEST["altura"];
        $g = 9.81;
        echo "tiempo = ", round((sqrt((2*$altura)/$g)), 2), " segundos.";
        ?>
        ');
        ?>
    </div>

    <!-- PIE DE PÁGINA -->
    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>