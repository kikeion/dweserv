<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3 -Ejercicio 18</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
</head>

<body>

    <div id="header">
        <h3>Tema 3: Sentencia condicional (if y switch). Ejercicio 18 - Resultado.</h3>
    </div>

    <div id="content">
        <?php
        $n = $_POST['num'];

        // si el número es negativo, se le cambia el signo:
        if ($n < 0) {
            $n = -$n;
        }

        if ($n < 10) {
            $digitos = 1;
        }

        if (($n >= 10) && ($n < 100)) {
            $digitos = 2;
        }

        if (($n >= 100) && ($n < 1000)) {
            $digitos = 3;
        }

        if (($n >= 1000) && ($n < 10000)) {
            $digitos = 4;
        }

        if ($n >= 10000) {
            $digitos = 5;
        }

        echo "El número introducido tiene $digitos dígitos.";
        ?>
        <br><br>
        <a href="ejercicio18.php">>> Volver</a>
    </div>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>