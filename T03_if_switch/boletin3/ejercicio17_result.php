<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3 -Ejercicio 17</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
</head>

<body>

    <div id="header">
        <h3>Tema 3: Sentencia condicional (if y switch). Ejercicio 17 - Resultado.</h3>
    </div>

    <div id="content">
        <?php
        $n = $_POST['num'];

        // si el número es negativo le cambiamos el signo:
        if ($n < 0) {
            $n = -$n;
        }

        if ($n < 10) {
            $primera = $n;
        }

        if (($n >= 10) && ($n < 100)) {
            $primera = $n / 10;
        }

        if (($n >= 100) && ($n < 1000)) {
            $primera = $n / 100;
        }

        if (($n >= 1000) && ($n < 10000)) {
            $primera = $n / 1000;
        }

        if ($n >= 10000) {
            $primera = $n / 10000;
        }
        // redondeamos al entero más bajo (floor):
        echo "La primera cifra del número introducido es ", floor($primera);
        ?>
        <br><br>
        <a href="ejercicio17.php">>> Volver</a>
    </div>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>