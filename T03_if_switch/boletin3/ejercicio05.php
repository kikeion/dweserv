<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3 -Ejercicio 5 Resultado</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
</head>

<body>

    <br>
    <?php
    $a = $_REQUEST['valor_a'];
    $b = $_REQUEST['valor_b'];
    if ($a == 0) {
        echo "x = NO TIENE SOLUCIÓN REAL";
    } else {
        echo "x = ", round((-$b / $a), 2);
    }
    ?>

    <br><br>
    <!-- BOTÓN VOLVER -->
    <button type="button" onclick="location.href='ejercicio05.html'">VOLVER</button> 
    <br>
    <hr>
    <br>
    <!-- CÓDIGO FUENTE -->
    <h5>Código fuente:</h5>
    <?php
    highlight_string('
    <?php
    $a = $_REQUEST["valor_a"];
    $b = $_REQUEST["valor_b"];
    if ($a == 0) {
        echo "x = NO TIENE SOLUCIÓN REAL";
    } else {
        echo "x = ", round((-$b / $a), 2);
    }
    ?>
        ');
    ?>
    <!-- PIE DE PÁGINA -->
    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>