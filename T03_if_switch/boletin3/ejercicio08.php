<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3 -Ejercicio 8 Resultado</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
    <!-- Amplía el programa anterior para que diga la nota del boletín (insuficiente, suficiente, bien, notable
        o sobresaliente). -->
</head>

<body>

    <br>
    <!-- CONTENIDO -->
    <main>

        <?php
        $boletin = "";
        $nota1 = $_REQUEST["nota1"];
        $nota2 = $_REQUEST["nota2"];
        $nota3 = $_REQUEST["nota3"];
        $promedio = round(($nota1 + $nota2 + $nota3) / 3, 2);
        // Uso if/else para que coja bien los posibles decimales
        if ($promedio < 5) {
            $boletin = "INSUFICIENTE";
        } elseif ($promedio >= 5 && $promedio < 6) {
            $boletin = "SUFICIENTE";
        } elseif ($promedio >= 6 && $promedio < 7) {
            $boletin = "BIEN";
        } elseif ($promedio >= 7 && $promedio < 9) {
            $boletin = "NOTABLE";
        } else {
            $boletin = "SOBRESALIENTE";
        }
        echo "La nota media es ", $promedio;
        echo "<br>Tiene un ", $boletin, " en el boletín";
        ?>

    </main>

    <br><br>
    <!-- BOTÓN VOLVER -->
    <button type="button" onclick="location.href='ejercicio08.html'">VOLVER</button>
    <br>
    <hr>
    <br>
    <!-- CÓDIGO FUENTE -->
    <div class="codigo">
        <h5>Código fuente:</h5>
        <?php
        highlight_string('
        <?php
        $boletin = "";
        $nota1 = $_REQUEST["nota1"];
        $nota2 = $_REQUEST["nota2"];
        $nota3 = $_REQUEST["nota3"];
        $promedio = round(($nota1 + $nota2 + $nota3) / 3, 2);
        // Uso if/else para que coja bien los posibles decimales
        if ($promedio < 5) {
            $boletin = "INSUFICIENTE";
        } elseif ($promedio >= 5 && $promedio < 6) {
            $boletin = "SUFICIENTE";
        } elseif ($promedio >= 6 && $promedio < 7) {
            $boletin = "BIEN";
        } elseif ($promedio >= 7 && $promedio < 9) {
            $boletin = "NOTABLE";
        } else {
            $boletin = "SOBRESALIENTE";
        }
        echo "La nota media es ", $promedio;
        echo "<br>Tiene un ", $boletin, " en el boletín";
        ?>
        ');
        ?>
    </div>

    <!-- PIE DE PÁGINA -->
    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>