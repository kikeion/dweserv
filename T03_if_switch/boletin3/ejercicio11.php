<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3 -Ejercicio 11 Resultado</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
    <!-- Escribe un programa que dada una hora determinada (horas y minutos), calcule los segundos que
        faltan para llegar a la medianoche. -->
</head>

<body>

    <br>

    <div id="content">

        <?php
        $hora = $_POST["hora"];
        $minuto = $_POST["minuto"];

        $segundosTranscurridos = ($hora * 3600) + ($minuto * 60);
        $segundosHastaMedianoche = (24 * 3600) - $segundosTranscurridos;

        echo "Desde las $hora:$minuto hasta la medianoche faltan ";
        echo "$segundosHastaMedianoche segundos.";
        ?>

        <br><br>
        <button type="button" onclick="location.href='ejercicio11.html'">VOLVER</button>

    </div>

    <br>
    <hr>
    <br>

    <div id="codigo">
        <h5>Código fuente:</h5>
        <?php
        highlight_string('
        <?php
        $hora = $_POST["hora"];
        $minuto = $_POST["minuto"];

        $segundosTranscurridos = ($hora * 3600) + ($minuto * 60);
        $segundosHastaMedianoche = (24 * 3600) - $segundosTranscurridos;

        echo "Desde las $hora:$minuto hasta la medianoche faltan ";
        echo "$segundosHastaMedianoche segundos.";
        ?>
        ');
        ?>
    </div>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>