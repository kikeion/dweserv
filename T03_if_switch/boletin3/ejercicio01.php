<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <title>Resultado ejercicio 1 - Tema 3</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <h4>A primera hora toca...</h4>
    <br>
    <?php
        $dia = $_REQUEST['diasemana'];
        switch($dia) {
            case 'lunes':
                echo ('Desarrollo Web Entorno Cliente');
            break;
            case 'martes':
                echo ('Desarrollo Web Entorno Cliente');
            break;
            case 'miercoles':
                echo ('Empresa e Iniciativa Emprendedora');
            break;
            case 'jueves':
                echo ('Desarrollo Web Entorno Servidor');
            break;
            case 'viernes':
                echo ('Desarrollo de Aplicaciones en Android');
            break;
            default:
            echo ('El día introducido no es correcto');
        }
    ?>

    <button type="button" onclick="location.href='ejercicio01.html'">VOLVER</button>
    <br>
    <hr>
    <br>
    <h5>Código fuente:</h5>
    <!-- MUESTRO EL CÓDIGO FUENTE CON highlight_string('código') -->
    <?php
    highlight_string('
    <?php
    $dia = $_REQUEST["diasemana"];
    switch($dia) {
        case "lunes":
            echo ("Desarrollo Web Entorno Cliente");
        break;
        case "martes":
            echo ("Desarrollo Web Entorno Cliente");
        break;
        case "miercoles":
            echo ("Empresa e Iniciativa Emprendedora");
        break;
        case "jueves":
            echo ("Desarrollo Web Entorno Servidor");
        break;
        case "viernes":
            echo ("Desarrollo de Aplicaciones en Android");
        break;
        default:
        echo ("El día introducido no es correcto");
    }
?>
    ');
    ?>


    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>
</body>

</html>