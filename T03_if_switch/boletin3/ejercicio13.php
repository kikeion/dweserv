<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3 -Ejercicio 13</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
    <!-- Escribe un programa que ordene tres números enteros introducidos por teclado. -->
</head>

<body>

    <div id="header">
        <h3>Tema 3: Sentencia condicional (if y switch). Ejercicio 13.</h3>
        <h2>Cuestionario</h2>
    </div>

    <div id="content">
        <form action="ejercicio13.php" method="post">
            Número 1 <input type="number" name="num1"><br>
            Número 2 <input type="number" name="num2"><br>
            Número 3 <input type="number" name="num3"><br>
            <input type="submit" value="Aceptar">
        </form>
        <?php
        if (isset($_POST["num1"]) || isset($_POST["num2"]) || isset($_POST["num3"])) {
            $a = $_POST["num1"];
            $b = $_POST["num2"];
            $c = $_POST["num3"];
            // se ordenan los dos primeros:
            if ($a > $b) {
                $aux = $a;
                $a = $b;
                $b = $aux;
            }
            // se ordenan de los dos últimos:
            if ($b > $c) {
                $aux = $b;
                $b = $c;
                $c = $aux;
            }
            // se vuelven a ordenar los dos primeros:
            if ($a > $b) {
                $aux = $a;
                $a = $b;
                $b = $aux;
            }
            echo "<br><div id='resultado'>Los números introducidos ordenados de menor a mayor son $a, $b y $c.</div>";
        }
        ?>

        <br><br>
        <a href="ejercicio13.php">VOLVER A INTENTAR</a>
    </div>

    <br>
    <hr>

    <div id="codigo">
        <h5>Código fuente:</h5>
        <?php
        highlight_string('
        <?php
        if (isset($_POST["num1"]) || isset($_POST["num2"]) || isset($_POST["num3"])) {
            $a = $_POST["num1"];
            $b = $_POST["num2"];
            $c = $_POST["num3"];
            // se ordenan los dos primeros:
            if ($a > $b) {
                $aux = $a;
                $a = $b;
                $b = $aux;
            }
            // se ordenan de los dos últimos:
            if ($b > $c) {
                $aux = $b;
                $b = $c;
                $c = $aux;
            }
            // se vuelven a ordenar los dos primeros:
            if ($a > $b) {
                $aux = $a;
                $a = $b;
                $b = $aux;
            }
            echo "<div id="resultado">Los números introducidos ordenados de menor a mayor son $a, $b y $c.</div>";
        }
        ?>
        ');
        ?>
    </div>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>