<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3 -Ejercicio 7 Resultado</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
    <!-- Realiza un programa que calcule la media de tres notas. -->
</head>

<body>

    <br>
    <!-- CONTENIDO -->
    <main>

        <?php
        $nota1 = $_REQUEST["nota1"];
        $nota2 = $_REQUEST["nota2"];
        $nota3 = $_REQUEST["nota3"];
        $promedio = round(($nota1 + $nota2 + $nota3) / 3, 2);
        echo "La nota media es ", $promedio;
        ?>

    </main>

    <br><br>
    <!-- BOTÓN VOLVER -->
    <button type="button" onclick="location.href='ejercicio07.html'">VOLVER</button>
    <br>
    <hr>
    <br>
    <!-- CÓDIGO FUENTE -->
    <div class="codigo">
        <h5>Código fuente:</h5>
        <?php
        highlight_string('
        <?php
        $nota1 = $_REQUEST["nota1"];
        $nota2 = $_REQUEST["nota2"];
        $nota3 = $_REQUEST["nota3"];
        $promedio = round(($nota1 + $nota2 + $nota3) / 3, 2);
        echo "La nota media es ", $promedio;
        ?>
        ');
        ?>
    </div>

    <!-- PIE DE PÁGINA -->
    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>