<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <title>Resultado ejercicio 2 - Tema 3</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
</head>

<body>

    <br>
    <?php
        $hora = $_REQUEST['hora'];
        if (($hora >= 6) && ($hora <= 12)) {
            echo ('<h2>Buenos días!</h2>');
        } else if (($hora >= 13) && ($hora <= 20)) {
            echo ('<h2>Buenas tardes!</h2>');
        } else {
            echo ('<h2>Buenas noches!</h2>');
        }    
    ?>

    <button type="button" onclick="location.href='ejercicio02.html'">VOLVER</button>
    <br>
    <hr>
    <br>
    <h5>Código fuente:</h5>
    <!-- MUESTRO EL CÓDIGO FUENTE CON highlight_string('código') -->
    <?php
    highlight_string('
    <?php
        $hora = $_REQUEST["hora"];
        if (($hora >= 6) && ($hora <= 12)) {
            echo ("<h2>Buenos días!</h2>");
        } else if (($hora >= 13) && ($hora <= 20)) {
            echo ("<h2>Buenas tardes!</h2>");
        } else {
            echo ("<h2>Buenas noches!</h2>");
        }    
    ?>
    ');
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>