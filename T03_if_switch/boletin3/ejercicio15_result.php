<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3 -Ejercicio 15</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
</head>

<body>

    <div id="header">
        <h3>Tema 3: Sentencia condicional (if y switch). Ejercicio 15 - Resultado.</h3>
    </div>

    <div id="content">
        <h2>Test de infidelidad</h2>
        <?php
        $puntos = 0;
        // por cada respuesta enviada va sumando los puntos en un acumulador:
        foreach ($_POST as $respuesta) {
            $puntos += $respuesta;
        }
        // Muestra el resultado del test
        echo "puntos obtenidos: ", $puntos, "<br>";

        if ($puntos <= 10) {
            echo "¡Enhorabuena! tu pareja parece ser totalmente fiel.";
        }

        if (($puntos > 11) && ($puntos <= 22)) {
            echo "Quizás exista el peligro de otra persona en su vida o en su mente,";
            echo "aunque seguramente será algo sin importancia. No bajes la guardia.";
        }

        if ($puntos >= 22) {
            echo "Tu pareja tiene todos los ingredientes para estar viviendo un ";
            echo "romance con otra persona. Te aconsejamos que indagues un poco más ";
            echo "y averigües qué es lo que está pasando por su cabeza.";
        }

        ?>
        <br><br>
        <a href="ejercicio15.php">>> Volver</a>


        <br><br>

    </div>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>