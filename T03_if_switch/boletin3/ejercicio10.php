<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3 -Ejercicio 10 Resultado</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
    <!-- Escribe un programa que nos diga el horóscopo a partir del día y el mes de nacimiento. -->
</head>

<body>

    <br>
    <!-- CONTENIDO -->
    <main>

        <?php

        $mes = $_POST["mes"];
        $dia = $_POST["dia"];

        switch ($mes) {
            case 1:
                if ($dia < 21) {
                    $horoscopo = "capricornio";
                } else {
                    $horoscopo = "acuario";
                }
                break;

            case 2:
                if ($dia < 20) {
                    $horoscopo = "acuario";
                } elseif ($dia >= 20 && $dia <= 29) {
                    $horoscopo = "piscis";
                } else {
                    $horoscopo = "La fecha de nacimiento es incorrecta!";
                }
                break;

            case 3:
                if ($dia < 21) {
                    $horoscopo = "piscis";
                } else {
                    $horoscopo = "aries";
                }
                break;

            case 4:
                if ($dia < 21) {
                    $horoscopo = "aries";
                } elseif ($dia >= 21 & $dia <= 30) {
                    $horoscopo = "tauro";
                } else {
                    $horoscopo = "La fecha de nacimiento es incorrecta!";
                }
                break;

            case 5:
                if ($dia < 20) {
                    $horoscopo = "tauro";
                } else {
                    $horoscopo = "géminis";
                }
                break;

            case 6:
                if ($dia < 22) {
                    $horoscopo = "géminis";
                } elseif ($dia >= 23 && $dia <= 30) {
                    $horoscopo = "cáncer";
                } else {
                    $horoscopo = "La fecha de nacimiento es incorrecta!";
                }
                break;

            case 7:
                if ($dia < 22) {
                    $horoscopo = "cáncer";
                } else {
                    $horoscopo = "Leo";
                }
                break;

            case 8:
                if ($dia < 24) {
                    $horoscopo = "leo";
                } else {
                    $horoscopo = "virgo";
                }
                break;

            case 9:
                if ($dia < 23) {
                    $horoscopo = "virgo";
                } else if ($dia >= 23 && $dia <= 30) {
                    $horoscopo = "libra";
                } else {
                    $horoscopo = "La fecha de nacimiento es incorrecta!";
                }
                break;

            case 10:
                if ($dia < 23) {
                    $horoscopo = "libra";
                } else {
                    $horoscopo = "escorpio";
                }
                break;

            case 11:
                if ($dia < 23) {
                    $horoscopo = "escorpio";
                } else if ($dia >= 23 && $dia <= 30) {
                    $horoscopo = "sagitario";
                } else {
                    $horoscopo = "La fecha de nacimiento es incorrecta!";
                }
                break;

            case 12:
                if ($dia < 21) {
                    $horoscopo = "sagitario";
                } else {
                    $horoscopo = "capricornio";
                }
                break;
        }

        echo "Tu horóscopo es <b>", $horoscopo, "</b>";
        ?>

    </main>

    <br><br>
    <!-- BOTÓN VOLVER -->
    <button type="button" onclick="location.href='ejercicio10.html'">VOLVER</button>
    <br>
    <hr>
    <br>
    <!-- CÓDIGO FUENTE -->
    <div class="codigo">
        <h5>Código fuente:</h5>
        <?php
        highlight_string('
        <?php

        $mes = $_POST["mes"];
        $dia = $_POST["dia"];

        switch ($mes) {
            case 1:
                if ($dia < 21) {
                    $horoscopo = "capricornio";
                } else {
                    $horoscopo = "acuario";
                }
                break;

            case 2:
                if ($dia < 20) {
                    $horoscopo = "acuario";
                } elseif ($dia >= 20 && $dia <= 29) {
                    $horoscopo = "piscis";
                } else {
                    echo "La fecha de nacimiento es incorrecta!";
                }
                break;

            case 3:
                if ($dia < 21) {
                    $horoscopo = "piscis";
                } else {
                    $horoscopo = "aries";
                }
                break;

            case 4:
                if ($dia < 21) {
                    $horoscopo = "aries";
                } elseif ($dia >= 21 & $dia <= 30) {
                    $horoscopo = "tauro";
                } else {
                    echo "La fecha de nacimiento es incorrecta!";
                }
                break;

            case 5:
                if ($dia < 20) {
                    $horoscopo = "tauro";
                } else {
                    $horoscopo = "géminis";
                }
                break;

            case 6:
                if ($dia < 22) {
                    $horoscopo = "géminis";
                } elseif ($dia >= 23 && $dia <= 30) {
                    $horoscopo = "cáncer";
                } else {
                    echo "La fecha de nacimiento es incorrecta!";
                }
                break;

            case 7:
                if ($dia < 22) {
                    $horoscopo = "cáncer";
                } else {
                    $horoscopo = "Leo";
                }
                break;

            case 8:
                if ($dia < 24) {
                    $horoscopo = "leo";
                } else {
                    $horoscopo = "virgo";
                }
                break;

            case 9:
                if ($dia < 23) {
                    $horoscopo = "virgo";
                } else if ($dia >= 23 && $dia <= 30) {
                    $horoscopo = "libra";
                } else {
                    echo "La fecha de nacimiento es incorrecta!";
                }
                break;

            case 10:
                if ($dia < 23) {
                    $horoscopo = "libra";
                } else {
                    $horoscopo = "escorpio";
                }
                break;

            case 11:
                if ($dia < 23) {
                    $horoscopo = "escorpio";
                } else if ($dia >= 23 && $dia <= 30) {
                    $horoscopo = "sagitario";
                } else {
                    echo "La fecha de nacimiento es incorrecta!";
                }
                break;

            case 12:
                if ($dia < 21) {
                    $horoscopo = "sagitario";
                } else {
                    $horoscopo = "capricornio";
                }
                break;
        }

        echo "Tu horóscopo es <b>", $horoscopo, "</b>";
        ?>
        ');
        ?>
    </div>

    <!-- PIE DE PÁGINA -->
    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>