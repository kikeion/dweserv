<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3 -Ejercicio 17</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
    <!-- Escribe un programa que diga cuál es la primera cifra de un número entero introducido por teclado.
        Se permiten números de hasta 5 cifras. -->
</head>

<body>

    <div id="header">
        <h3>Tema 3: Sentencia condicional (if y switch). Ejercicio 17.</h3>
    </div>

    <div id="content">
    Introduce un número entero (máximo 5 cifras): 
        <form action="ejercicio17_result.php" method="post">
          <input type="number" min="-99999" max="99999" name="num" autofocus required><br>
          <input type="submit" value="Aceptar">
        </form>
    </div>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>