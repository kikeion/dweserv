<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3 -Ejercicio 16</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
    <!-- Escribe un programa que diga cuál es la última cifra de un número entero introducido por teclado. -->
</head>

<body>

    <div id="header">
        <h3>Tema 3: Sentencia condicional (if y switch). Ejercicio 16.</h3>
    </div>

    <div id="content">
    Por favor, introduce un número entero: 
        <form action="ejercicio16_result.php" method="post">
          <input type="number" name="num" autofocus required><br>
          <input type="submit" value="Aceptar">
        </form>
    </div>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>