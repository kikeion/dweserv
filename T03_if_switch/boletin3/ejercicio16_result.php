<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3 -Ejercicio 16</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
</head>

<body>

    <div id="header">
        <h3>Tema 3: Sentencia condicional (if y switch). Ejercicio 16 - Resultado.</h3>
    </div>

    <div id="content">

        <?php
        $n = $_POST['num'];
        echo "La última cifra de $n es ", $n % 10;
        ?>
        <br><br>
        <a href="ejercicio16.php">>> Volver</a>

    </div>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>