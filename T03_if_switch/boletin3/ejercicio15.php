<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3 -Ejercicio 15</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
    <!-- Realiza un programa que nos diga si hay probabilidad de que nuestra pareja nos está siendo
        infiel. El programa irá haciendo preguntas que el usuario contestará con verdadero o falso. Puedes
        utilizar radio buttons. Cada pregunta contestada como verdadero sumará 3 puntos. Las preguntas
        contestadas con falso no suman puntos. Utiliza el fichero test_infidelidad.txt² para obtener las
        preguntas y las conclusiones del programa. -->
</head>

<body>

    <div id="header">
        <h3>Tema 3: Sentencia condicional (if y switch). Ejercicio 15.</h3>
    </div>

    <div id="content">
        <h2>Test de infidelidad</h2>

        <form action="ejercicio15_result.php" method="post">
            <ol>
                <li>
                    Tu pareja parece estar más inquieta de lo normal sin ningún motivo
                    aparente.<br>
                    <input type="radio" name="p1" value="3" required>verdadero<br>
                    <input type="radio" name="p1" value="0">falso<br>
                </li>

                <li>
                    Ha aumentado sus gastos de vestuario.<br>
                    <input type="radio" name="p2" value="3">verdadero<br>
                    <input type="radio" name="p2" value="0">falso<br>
                </li>

                <li>
                    Ha perdido el interés que mostraba anteriormente por ti.<br>
                    <input type="radio" name="p3" value="3">verdadero<br>
                    <input type="radio" name="p3" value="0">falso<br>
                </li>

                <li>
                    Ahora se afeita y se asea con más frecuencia (si es hombre) o ahora se
                    arregla el pelo y se asea con más frecuencia (si es mujer).<br>
                    <input type="radio" name="p4" value="3">verdadero<br>
                    <input type="radio" name="p4" value="0">falso<br>
                </li>

                <li>
                    No te deja que mires la agenda de su teléfono móvil.<br>
                    <input type="radio" name="p5" value="3">verdadero<br>
                    <input type="radio" name="p5" value="0">falso<br>
                </li>

                <li>
                    A veces tiene llamadas que dice no querer contestar cuando estás
                    tú delante.<br>
                    <input type="radio" name="p6" value="3">verdadero<br>
                    <input type="radio" name="p6" value="0">falso<br>
                </li>

                <li>
                    Últimamente se preocupa más en cuidar la línea y/o estar bronceado/a.<br>
                    <input type="radio" name="p7" value="3">verdadero<br>
                    <input type="radio" name="p7" value="0">falso<br>
                </li>

                <li>
                    Muchos días viene tarde después de trabajar porque dice tener mucho
                    más trabajo.<br>
                    <input type="radio" name="p8" value="3">verdadero<br>
                    <input type="radio" name="p8" value="0">falso<br>
                </li>

                <li>
                    Has notado que últimamente se perfuma más.<br>
                    <input type="radio" name="p9" value="3">verdadero<br>
                    <input type="radio" name="p9" value="0">falso<br>
                </li>

                <li>
                    Se confunde y te dice que ha estado en sitios donde no ha ido contigo.<br>
                    <input type="radio" name="p10" value="3">verdadero<br>
                    <input type="radio" name="p10" value="0">falso<br>
                </li>
            </ol>
            <input type="submit" value="Aceptar">
        </form>

        <br><br>

    </div>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>