<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3 -Ejercicio 19</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
    <!-- Realiza un programa que diga si un número entero positivo introducido por teclado es capicúa. Se
        permiten números de hasta 5 cifras. -->
</head>

<body>

    <div id="header">
        <h3>Tema 3: Sentencia condicional (if y switch). Ejercicio 19.</h3>
    </div>

    <div id="content">
    Introduce un número entero (máximo 5 cifras): 
        <form action="ejercicio19_result.php" method="post">
          <input type="number" min="-99999" max="99999" name="num" autofocus required><br>
          <input type="submit" value="Aceptar">
        </form>
    </div>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>