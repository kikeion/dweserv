<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3 -Ejercicio 9 Resultado</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
    <!-- Realiza un programa que resuelva una ecuación de segundo grado (del tipo ax2 + bx + c = 0). -->
</head>

<body>

    <br>
    <!-- CONTENIDO -->
    <main>

        <?php

        $a = $_REQUEST["valor_a"];
        $b = $_REQUEST["valor_b"];
        $c = $_REQUEST["valor_c"];

        // 0x^2 + 0x + 0 = 0

        if (($a == 0) && ($b == 0) && ($c == 0)) {
            echo  "La ecuación tiene infinitas soluciones.";
        }


        // 0x^2 + 0x + c = 0  con c distinto de 0

        if (($a == 0) && ($b == 0) && ($c != 0)) {
            echo  "La ecuación no tiene solución.";
        }


        // ax^2 + bx + 0 = 0  con a y b distintos de 0

        if (($a != 0) && ($b != 0) && ($c == 0)) {
            echo  "x<sub>1</sub> = 0";
            echo  "<br>x<sub>2</sub> = ", (-$b / $a);
        }


        // 0x^2 + bx + c = 0  con b y c distintos de 0

        if (($a == 0) && ($b != 0) && ($c != 0)) {
            echo  "x<sub>1</sub> = x<sub>2</sub> = ", (-$c / $b);
        }


        // ax^2 + bx + c = 0  con a, b y c distintos de 0

        if (($a != 0) && ($b != 0) && ($c != 0)) {

            $discriminante = ($b * $b) - (4 * $a * $c);

            if ($discriminante < 0) {
                echo  "La ecuación no tiene soluciones reales";
            } else {
                echo  "x<sub>1</sub> = ", (-$b + sqrt($discriminante)) / (2 * $a);
                echo  "<br>x<sub>2</sub> = ", (-$b - sqrt($discriminante)) / (2 * $a);
            }
        }
        ?>

    </main>

    <br><br>
    <!-- BOTÓN VOLVER -->
    <button type="button" onclick="location.href='ejercicio09.html'">VOLVER</button>
    <br>
    <hr>
    <br>
    <!-- CÓDIGO FUENTE -->
    <div class="codigo">
        <h5>Código fuente:</h5>
        <?php
        highlight_string('
        <?php
        
        $a = $_REQUEST["valor_a"];
        $b = $_REQUEST["valor_b"];
        $c = $_REQUEST["valor_c"];

        // 0x^2 + 0x + 0 = 0

        if (($a == 0) && ($b == 0) && ($c == 0)) {
            echo  "La ecuación tiene infinitas soluciones.";
        }

        // 0x^2 + 0x + c = 0  con c distinto de 0

        if (($a == 0) && ($b == 0) && ($c != 0)) {
            echo  "La ecuación no tiene solución.";
        }

        // ax^2 + bx + 0 = 0  con a y b distintos de 0

        if (($a != 0) && ($b != 0) && ($c == 0)) {
            echo  "x<sub>1</sub> = 0";
            echo  "<br>x<sub>2</sub> = ", (-$b / $a);
        }

        // 0x^2 + bx + c = 0  con b y c distintos de 0

        if (($a == 0) && ($b != 0) && ($c != 0)) {
            echo  "x<sub>1</sub> = x<sub>2</sub> = ", (-$c / $b);
        }

        // ax^2 + bx + c = 0  con a, b y c distintos de 0

        if (($a != 0) && ($b != 0) && ($c != 0)) {

            $discriminante = ($b * $b) - (4 * $a * $c);

            if ($discriminante < 0) {
                echo  "La ecuación no tiene soluciones reales";
            } else {
                echo  "x<sub>1</sub> = ", (-$b + sqrt($discriminante)) / (2 * $a);
                echo  "<br>x<sub>2</sub> = ", (-$b - sqrt($discriminante)) / (2 * $a);
            }
        }

        ?>
        ');
        ?>
    </div>

    <!-- PIE DE PÁGINA -->
    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>