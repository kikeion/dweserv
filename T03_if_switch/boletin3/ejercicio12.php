<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3 -Ejercicio 12</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
    <!-- Realiza un minicuestionario con 10 preguntas tipo test sobre las asignaturas que se imparten en
        el curso. Cada pregunta acertada sumará un punto. El programa mostrará al final la calificación
        obtenida. Pásale el minicuestionario a tus compañeros y pídeles que lo hagan para ver qué tal andan
        de conocimientos en las diferentes asignaturas del curso. -->
</head>

<body>

    <div id="header">
        <h3>Tema 3: Sentencia condicional (if y switch). Ejercicio 12.</h3>
    </div>

    <div id="content">
        <h2>Cuestionario</h2>

        <form action="ejercicio12.php" method="post">
            <ol>
                <li>
                    Mezcla de lenguaje natural con algunas convenciones sintácticas propias de lenguajes de programación<br>
                    <input type="radio" name="p1" value="0">a) diagrama de flujo<br>
                    <input type="radio" name="p1" value="1">b) pseudocódigo<br>
                    <input type="radio" name="p1" value="0">c) sintaxis<br>
                </li>

                <li>
                    Proceso de diseñar, codificar, depurar y mantener el código fuente de programas computacionales.<br>
                    <input type="radio" name="p2" value="0">a) compilación<br>
                    <input type="radio" name="p2" value="0">b) pruebas del software<br>
                    <input type="radio" name="p2" value="1">c) programación<br>
                </li>

                <li>
                    Conjunto de líneas de texto que son las instrucciones que debe seguir la computadora para ejecutar dicho programa.<br>
                    <input type="radio" name="p3" value="0">a) algoritmo<br>
                    <input type="radio" name="p3" value="1">b) código fuente<br>
                    <input type="radio" name="p3" value="0">c) bloque<br>
                </li>

                <li>
                    Lenguaje formal diseñado para expresar procesos que pueden ser llevados a cabo por máquinas como las computadoras.<br>
                    <input type="radio" name="p4" value="0">a) código<br>
                    <input type="radio" name="p4" value="0">b) lenguaje binario<br>
                    <input type="radio" name="p4" value="1">c) lenguaje de programación<br>
                </li>

                <li>
                    Conjunto prescrito de instrucciones o reglas bien definidas, ordenadas y finitas que permite realizar una actividad mediante pasos sucesivos que no generen dudas a quien deba realizar dicha actividad.<br>
                    <input type="radio" name="p5" value="1">a) algoritmo<br>
                    <input type="radio" name="p5" value="0">b) ciclos<br>
                    <input type="radio" name="p5" value="0">c) estructura<br>
                </li>
            </ol>
            <input type="submit" value="Aceptar">
        </form>
        <?php
        if (isset($_POST["p1"]) || isset($_POST["p2"]) || isset($_POST["p3"]) || isset($_POST["p4"]) || isset($_POST["p5"])) {
            $puntos = $_POST["p1"] + $_POST["p2"] + $_POST["p3"] + $_POST["p4"] + $_POST["p5"];
            echo "<div><p>Ha obtenido $puntos puntos.</p></div>";
        }
        ?>

        <br><br>
        <a href="ejercicio12.php">VOLVER A INTENTAR</a>
    </div>

    <br>
    <hr>
    <br>

    <div id="codigo">
        <h5>Código fuente:</h5>
        <?php
        highlight_string('
        <?php
        if (isset($_POST["p1"]) || isset($_POST["p2"]) || isset($_POST["p3"]) || isset($_POST["p4"]) || isset($_POST["p5"])) {
            $puntos = $_POST["p1"] + $_POST["p2"] + $_POST["p3"] + $_POST["p4"] + $_POST["p5"];
            echo "<div><p>Ha obtenido $puntos puntos.</p></div>";
        }
        ?>
        ');
        ?>
    </div>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>