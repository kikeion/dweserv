<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3 -Ejercicio 19</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
</head>

<body>

    <div id="header">
        <h3>Tema 3: Sentencia condicional (if y switch). Ejercicio 19 - Resultado.</h3>
    </div>

    <div id="content">
        <?php
        $n = $_POST['num'];

        $capicua = false;

        // comparamos primera y última cifra con cociente redondeado y resto(ejercicos 16 y 17):

        // una cifra:
        if ($n < 10) {
            $capicua = true;
        }

        // dos cifras:
        if (($n >= 10) && ($n < 100)) {
            if (floor($n / 10) == ($n % 10)) {
                $capicua = true;
            }
        }

        // tres cifras:
        if (($n >= 100) && ($n < 1000)) {
            if (floor($n / 100) == ($n % 10)) {
                $capicua = true;
            }
        }

        // cuatro cifras:
        if (($n >= 1000) && ($n < 10000)) {
            if ((floor($n / 1000) == ($n % 10)) && ((floor($n / 100) % 10) == (floor($n / 10) % 10))) {
                $capicua = true;
            }
        }

        // inco cifras:
        if ($n >= 10000) {
            if ((floor($n / 10000) == ($n % 10)) && (((floor($n / 1000) % 10)) == (floor($n / 10) % 10))) {
                $capicua = true;
            }
        }

        if ($capicua) {
            echo "El número introducido es capicúa.";
        } else {
            echo "El número introducido no es capicúa.";
        }
        ?>
        <br><br>
        <a href="ejercicio19.php">>> Volver</a>
    </div>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>