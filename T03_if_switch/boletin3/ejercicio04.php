<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <title>Resultado ejercicio 4 - Tema 3</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
</head>

<body>

    <br>
    <?php
    $horas = $_POST['horas'];
    $salario = 0;
    if ($horas > 40) {
        $salario = ($horas - 40) * 16 + 480;
    } else {
        $salario = $horas * 12;
    }
    echo "HORAS TRABAJADAS: ", $horas, " h<br>";
    echo "PRECIO HORA: 12 euros<br>";
    if ($horas > 40) {
        echo "HORAS EXTRA: ", $horas - 40, " h<br>";
    } else {
        echo "HORAS EXTRA: 0<br>";
    }    
    echo "PRECIO HORA EXTRA: 16 euros<br>";
    echo "<b>SALARIO SEMANAL = ", $salario, " euros</b>";
    ?>

    <br><br>
    <button type="button" onclick="location.href='ejercicio04.html'">VOLVER</button> <!-- botón volver -->
    <br>
    <hr>
    <br>
    <h5>Código fuente:</h5>

    <?php
    highlight_string('
    <?php
    $horas = $_POST["horas"];
    $salario = 0;
    if ($horas > 40) {
        $salario = ($horas - 40) * 16 + 480;
    } else {
        $salario = $horas * 12;
    }
    echo "HORAS TRABAJADAS: ", $horas, " h<br>";
    echo "PRECIO HORA: 12 euros<br>";
    if ($horas > 40) {
        echo "HORAS EXTRA: ", $horas - 40, " h<br>";
    } else {
        echo "HORAS EXTRA: 0<br>";
    }    
    echo "PRECIO HORA EXTRA: 16 euros<br>";
    echo "<b>SALARIO SEMANAL = ", $salario, " euros</b>";
    ?>
        ');
    ?>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>