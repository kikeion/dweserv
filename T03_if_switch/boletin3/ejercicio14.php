<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3 -Ejercicio 14</title>
    <link href="default.css" rel="stylesheet" type="text/css" />
    <!-- Realiza un programa que diga si un número introducido por teclado es par y/o divisible entre 5. -->
</head>

<body>

    <div id="header">
        <h3>Tema 3: Sentencia condicional (if y switch). Ejercicio 14.</h3>
    </div>

    <div id="content">
        <label for="formulario">Introduce un número para saber si es par y/o divisible entre 5:</label>
        <form action="ejercicio14.php" name="formulario" method="post"><br>
            <input type="number" name="numero" autofocus required><br>
            <input type="submit" value="Aceptar">
        </form>
        <?php
        if (isset($_POST["numero"])) {
            $num = $_POST["numero"];
            if ($num % 2 == 0) {
                echo "<br><div>El número ", $num, " es PAR ";
            } else {
                echo "<br><div>El número ", $num, " es IMPAR ";
            }
            if ($num % 5 == 0) {
                echo "y divisible entre 5.</div>";
            } else {
                echo "y NO es divisible entre 5.</div>";
            }
        }
        ?>
    </div>

    <br>
    <hr>

    <div id="codigo">
        <h5>Código fuente:</h5>
        <?php
        highlight_string('
            <form action="ejercicio14.php" method="post">
            Número 1 <input type="number" name="numero"><br>
            <input type="submit" value="Aceptar">
            </form>
        <?php
        if (isset($_POST["numero"])) {
            $num = $_POST["numero"];
            if ($num % 2 == 0) {
                echo "<br><div>El número ", $num, " es PAR ";
            } else {
                echo "<br><div>El número ", $num, " es IMPAR ";
            }
            if ($num % 5 == 0) {
                echo "y divisible entre 5.</div>";
            } else {
                echo "y NO es divisible entre 5.</div>";
            }
        }
        ?>
        ');
        ?>
    </div>

    <div id="footer">
        <hr>
        <p>&copy; David Benítez Cabeza - 2ºDAW 2020/21</p>
    </div>

</body>

</html>