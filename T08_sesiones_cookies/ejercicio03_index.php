<?php
session_start();

if (!isset($_SESSION['productos'])) {
    $_SESSION['productos'] = array(
        "dilatador" => array(
            "nombre" => "Dilatador de sonrisas 'WhySoSerious?'",
            "precio" => 19.99,
            "imagen" => 'dilatadorsonrisas.jpg',
            "detalle" => "Consigue una sonrisa perfecta que el mismísimo Jocker envidiaría. Sólo hay que llevarla 20 horas al día."
        ),
        "enfriador" => array(
            "nombre" => "Enfriador de sandías 'CoolFruit 9000'",
            "precio" => 69.99,
            "imagen" => 'enfriadorsandias.jpg',
            "detalle" => "Para que puedas llevar tu sandía fresquita a cualquier parte, incluso a Marte!!"
        ),
        "mascara" => array(
            "nombre" => "Máscara de belleza facial 'ScaryMasky'",
            "precio" => 49.99,
            "imagen" => 'mascarabelleza.jpg',
            "detalle" => "Máscara que mejora tu belleza y tus fiestas de haloween."
        ),
        "protector" => array(
            "nombre" => "Protector de espías para PC 'Me&myself'",
            "precio" => 14.99,
            "imagen" => 'protectorespias.jpg',
            "detalle" => 'Nadie podrá ver lo que haces en tu ordenador. Tú no podrás ver lo que pasa fuera.'
        ),
        "subidor" => array(
            "nombre" => "Subidor de calcetines 'Sockamatic 300'",
            "precio" => 9.99,
            "imagen" => 'subidorcalcetines.jpg',
            "detalle" => "Ideal para esos calcetines que llevan varios lavados."
        )
    );
    // si existe la cookie 'cesta', unserialize para almacenar el array en forma de texto en la sesión 'enCesta'
    // y recupero el total y la cantidad para guardarlos en sus respectivas sesiones:
    if (isset($_COOKIE['cesta'])) {
        $_SESSION['enCesta'] = unserialize($_COOKIE['enCesta']);
        $_SESSION['total']    = $_COOKIE['total'];
        $_SESSION['cantidad'] = $_COOKIE['cantidad'];
    }
}
// si no existe la sesión de productos en la cesta, inicializo array de productos en la cesta, el total y la cantidad:
if (!isset($_SESSION['enCesta'])) {
    $_SESSION['enCesta'] = [];
    $_SESSION['total'] = 0;
    $_SESSION['cantidad'] = 0;
}
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>T8:Sesiones y cookies. Ej3-index</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header>
        <h1>TIENDA ONLINE: AMAZING PRODUCTS</h1>
    </header>
    <container id='contenido'>
        <table id='productos'>
            <tr>
                <td colspan="4">Estás en la página principal</td>
                <td>CESTA <br>total productos =<?= $_SESSION['cantidad'] ?><br><a href="ejercicio03_cesta.php">Ver cesta</a></td>
            </tr>
            <?php
            // dibujamos la tabla con formulario para añadir productos a la cesta a través de meteCesta.php:
            foreach ($_SESSION['productos'] as $producto => $elemento) {
                echo "<tr><td><img src='img/" . $elemento['imagen'] . "'/><td>" . $elemento['nombre'] . "</td><td><a href='ejercicio03_detalle.php?producto=" . $producto . "'>Detalle</a></td><td>$" . $elemento['precio'] . "</td></td>";
                echo "<td><form action='ejercicio03_meteCesta.php' method='POST'>
                <input type='hidden' name='producto' value='" . $producto . "'> 
                <input type='submit' value='Añadir a la cesta'>
                </form></td></tr>";
            }
            ?>
        </table>
    </container>
</body>

</html>