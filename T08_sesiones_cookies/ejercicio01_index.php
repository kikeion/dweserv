<?php
session_start(); // Inicio de sesión
// si recibe parámetro 'reset' destruye la sesión para comenzar nueva paleta y refresca la página
if (isset($_REQUEST['reset'])) {
    session_destroy();
    header("refresh:0;");
}
if (!isset($_SESSION['colores'])) {
    $_SESSION['colores'] = [];
}
if (isset($_REQUEST['color'])) {
    $color = generaColor();
    $_SESSION['colores'][] = $color;
    echo "<style>body{background-color: RGB(", $color[0], ",", $color[1], ",", $color[2], ");}</style>";
} else {
    $color = "";
}
function generaColor()
{
    $color = [];
    for ($i = 0; $i < 3; $i++) {
        $color[] = random_int(0, 255);
    }
    return $color;
}

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 8: Sesiones y cookies - Ejercicio 1</title>
    <!-- Crear una página principal con un botón 'Añadir color' para generar un color aleatorio que además se 
    establecerá como color de fondo de la página, cada vez que se pulsa irá generando un color nuevo (actualizando el fondo), 
    que se irán almacenando en un array de sesión. Habrá un segundo botón 'Mostrar paleta creada' que dirige a una segunda página 
    que mostrará una paleta con los colores generados. Esta paleta no es más que una tabla con un máximo de 5 celdas por cada fila, 
    y en cada celda se muestra un color de los generados. Debajo de la tabla tendremos 2 botones uno para volver a la página principal 
    y seguir añadiendo colores a la paleta y otro para destruir la sesión y generar una paleta nueva. -->

</head>
<!-- doy color al body según voy añadiendo colores -->

<body style="background-color:rgb(<?= $color[0] ?>,<?= $color[1] ?>,<?= $color[2] ?>)>;">
    <?php
    echo "<p>Colores creados = ", count($_SESSION['colores']), "</p>"; // contamos los colores
    ?>
    <br>
    <form action="#" method="POST">
        <input type="submit" name="color" value="Añadir color">
    </form>
    <br>
    <form action="ejercicio01_2.php" method="POST">
        <input type="submit" name="paleta" value="Mostrar paleta creada">
    </form>

</body>

</html>