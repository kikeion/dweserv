<?php
session_start();

if (isset($_POST['quitar'])) {
    $producto = $_POST['quitar'];
    $_SESSION['enCesta'][$producto]--;
    if ($_SESSION['enCesta'][$producto] == 0) {
        unset($_SESSION['enCesta'][$producto]);
    }
    $_SESSION['cantidad']--;
    $_SESSION['total'] -= $_SESSION['productos'][$producto]['precio'];
    setcookie('cantidad', $_SESSION['cantidad'], time() + 24 * 3600);
    setcookie('total', $_SESSION['total'], time() + 24 * 3600);
    setcookie('enCesta', serialize($_SESSION['enCesta']), time() + 24 * 3600);
}

header('Location:ejercicio03_cesta.php');
