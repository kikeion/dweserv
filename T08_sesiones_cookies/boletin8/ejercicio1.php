<?php
session_start();
if (!isset($_SESSION['numeros'])) {
    $_SESSION['numeros'] = [];
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Boletin Tema 8 - Ejercicio 1</title>
    <!-- Escribe un programa que calcule la media de un conjunto de números positivos introducidos por
        teclado. A priori, el programa no sabe cuántos números se introducirán. El usuario indicará que ha
        terminado de introducir los datos cuando meta un número negativo. Utiliza sesiones. -->
</head>

<body>
    <h2>Tema 8: sesiones y cookies - Ejercicio 1.</h2>
    <form action="ejercicio1.php" method="post">
        <label for="num">Introduce números para calcular la media (negativo para finalizar):</label>
        <input type="number" name="num" id="num" required>
        <input type="submit" value="Enviar">
    </form>

    <?php
    if (isset($_REQUEST['num']) && $_REQUEST['num'] > 0) {
        $num = $_REQUEST['num'];
        $_SESSION['numeros'][] = $num;
    } else if (isset($_REQUEST['num']) && $_REQUEST['num'] < 0) {
        $suma = 0;
        $media = 0;
        for ($i = 0; $i < count($_SESSION['numeros']); $i++) {
            $suma += $_SESSION['numeros'][$i];
        }
        $media = $suma / count($_SESSION['numeros']);
        echo "<p>La media de los números introducidos es: ", $media, "</p>";
        session_destroy();
    }

    ?>



</body>

</html>