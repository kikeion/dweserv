<?php
session_start();

// si recibe el producto, mete el nombre en la variable producto y lo busca en la sesión enCesta
if (isset($_POST['producto'])) {
    $producto = $_POST['producto'];
    if (array_key_exists($producto, $_SESSION['enCesta'])) {
        $_SESSION['enCesta'][$producto]++;
    }else{
        $_SESSION['enCesta'][$producto]=1;   
    }
    
    $_SESSION['cantidad']++;
    $_SESSION['total'] += $_SESSION['productos'][$producto]['precio'];
    setcookie('cantidad', $_SESSION['cantidad'], time() + 24 * 3600);
    setcookie('total', $_SESSION['total'], time() + 24 * 3600);
    setcookie('enCesta', serialize($_SESSION['enCesta']), time() + 24 * 3600);
}
header('Location:ejercicio03_index.php');// envía a la página principal
