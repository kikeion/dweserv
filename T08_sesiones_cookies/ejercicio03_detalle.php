<?php
// recupero la sesión:
session_start();
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>T8:Sesiones y cookies. Ej3-detalle</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header>
        <h1>TIENDA ONLINE: AMAZING PRODUCTS</h1>
    </header>
    <container id='contenido'>
        <h2>Detalle del producto:</h2>
        <div id="detalle">
            <!-- detalle del producto recogido como parámetro en el enlace -->
            <img src="img/<?= $_SESSION['productos'][$_REQUEST['producto']]['imagen'] ?>" />
            <p><b><?= $_SESSION['productos'][$_REQUEST['producto']]['nombre'] ?></b></p>
            <p><?= $_SESSION['productos'][$_REQUEST['producto']]['detalle'] ?></p>
            <p>$<?= $_SESSION['productos'][$_REQUEST['producto']]['precio'] ?></p>
        </div>
        <form action="ejercicio03_meteCesta.php" method="POST">
            <input type="hidden" name="producto" value="<?= $_REQUEST['producto'] ?>">
            <input type="submit" value="Comprar">
        </form>
        <a href="ejercicio03_index.php">VOLVER</a>
    </container>
</body>

</html>