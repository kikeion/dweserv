<!-- DECLARACIÓN INICIO -->
<?php
session_start();
// para resetear la encuesta:
if (isset($_REQUEST['reset'])) {
    setcookie("swars", NULL, -1);
    setcookie("strek", NULL, -1);
    unset($_COOKIE['swars']);
    unset($_COOKIE['strek']);
    unset($_SESSION['swars']);
    unset($_SESSION['strek']);
}
// existe la sesión?? primera vez que entro:
if (!isset($_SESSION['swars']) || !isset($_SESSION['strek'])) {
    // voy a comprobar si existe una cookie con datos:
    if (isset($_COOKIE['swars']) || isset($_COOKIE['strek'])) {
        // si existe recupero los datos de la cookie:
        $_SESSION['swars'] = $_COOKIE['swars'];
        $_SESSION['strek'] = $_COOKIE['strek'];
    } else {
        // si no existe la cookie, creo la sesión vacía:
        $_SESSION['swars'] = 0;
        $_SESSION['strek'] = 0;
    }
}
// si existe el parámetro (voto) incremento en 1 la sesión
if (isset($_REQUEST['swars'])) {
    $_SESSION['swars']++;
    // seteo la cookie con la nueva información (para la sesión siguiente)
    setcookie('swars', $_SESSION['swars'], time() + 60 * 60 * 24 * 30);
}
if (isset($_REQUEST['strek'])) {
    $_SESSION['strek']++;
    setcookie('strek', $_SESSION['strek'], time() + 60 * 60 * 24 * 30);
}

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 8: Sesiones y cookies - Ejercicio02_index</title>
    <!-- Crear una página que simula una encuesta. Se realizará una pregunta, con dos botones para responder, 
        cada vez que se pulse un botón se irán contabilizando (usa sesiones) los votos y actualizando una barra que muestre 
        el número de votos de cada respuesta. Este resultado se irá almacenando también en una cookie, de manera que si se cierra 
        el navegador, al abrir la página de nuevo se mostrarán los resultados hasta el momento en que se cerró. Crear la cookie 
        para 3 meses. -->
    <style>
        body {
            font-family: 'Segoe UI', 'Open Sans', 'Helvetica Neue', sans-serif;
        }
    </style>
</head>

<body>
    <h2>¿Qué saga es mejor? Vota!</h2>
    <!-- STAR WARS -->
    <div id="starwars">
        <div class="logo">
            <img src="img/Star-Wars-Logo.png" width="150px" alt="strwars">
            <?php
            for ($i = 0; $i < $_SESSION['swars']; $i++) {
                echo "<img src='img/trooper.png' width='40px' alt='trooper'>";
            }
            ?>
        </div>
    </div>
    <!-- STAR TREK -->
    <div id="startrek">
        <div class="logo">
            <img src="img/startreklogo.png" width="200px" alt="strtrek">
            <?php
            for ($i = 0; $i < $_SESSION['strek']; $i++) {
                echo "<img src='img/spock.png' width='50px' alt='spock'>";
            }
            ?>
        </div>
    </div>
    <!-- FORMULARIO -->
    <form action="ejercicio02_index.php" method="POST">
        <input type="submit" name="swars" value="Star Wars">
        <input type="submit" name="strek" value="Star Trek">
        <br><input type="submit" name="reset" value="Reiniciar la encuesta">
    </form>
    <?php
    echo "<p>TOTAL STAR WARS: " . $_SESSION['swars'] . "</p>";
    echo "<p>TOTAL STAR TREK: " . $_SESSION['strek'] . "</p>";
    ?>
</body>

</html>