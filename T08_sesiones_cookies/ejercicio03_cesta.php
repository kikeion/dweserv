<?php
session_start();
if (!isset($_SESSION['enCesta'])) {
    $_SESSION['enCesta'] = ['dilatador' => 0, 'enfriador' => 0, 'mascara' => 0, 'protector' => 0, 'subidor' => 0];
    $_SESSION['total'] = 0;
    $_SESSION['cantidad'] = 0;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>T8:Sesiones y cookies. Ej3-cesta</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header>
        <h1>TIENDA ONLINE: AMAZING PRODUCTS</h1>
    </header>

    <container id='contenido'>
        <table id='productos'>
            <tr>
                <th colspan="2">Tu cesta de la compra:</th>
                <th>CANTIDAD</th>
                <th>PRECIO</th>
                <th>OPCIONES</th>
            </tr>
            <?php
            foreach ($_SESSION['enCesta'] as $producto => $cantidad) {
                if ($cantidad > 0) {
                    echo "<tr><td>" . $_SESSION['productos'][$producto]['nombre'] . "</td><td><img src='img/" . $_SESSION['productos'][$producto]['imagen']  . "'/>
                    <td>" . $cantidad . "</td></td><td>$" . $_SESSION['productos'][$producto]['precio'] . "</td>";
                    // para eliminar productos, dirige a quitaCesta.php enviando producto:
                    echo "<td><form action='ejercicio03_quitaCesta.php' method='POST'>
                    <input type='hidden' name='quitar' value='" . $producto . "'>
                    <input type='submit' value='Eliminar'>
                    </form></td></tr>";
                }
            }
            echo "<tr><td colspan='2'>Total</td><td>" . $_SESSION['cantidad'] . "</td><td>$"
                . $_SESSION['total'] . "</td><td><a href='ejercicio03_index.php'>Volver</a></td></tr></table>";

            ?>
        </table>
    </container>
</body>

</html>