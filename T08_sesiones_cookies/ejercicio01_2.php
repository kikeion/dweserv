<!-- recogemos la sesión -->
<?php
session_start();
// si no he añadido ningún color me redirige a la primera página:
if (!isset($_SESSION['colores'])) {
    header("location:ejercicio01_index.php");
}
// funcionalidad que cambia de color el fondo cuando pinchamos en la paleta:
if (isset($_REQUEST['fondo'])) {
    $fondo = $_REQUEST['fondo'];
} else {
    $fondo = "";
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 8: Sesiones y cookies - Ejercicio 1 - p2</title>
    <style>
        table td {
            height: 25px;
            width: 25px;
        }
    </style>
</head>

<body style="background-color:rgb(<?= $fondo ?>);">
    <?php
    // imprimo tabla
    $contador = 0;
    echo "<table id='paleta'><tr>";
    foreach ($_SESSION['colores'] as $indice => $valor) {
        echo "<td style='background-color:RGB(", $valor[0], ",", $valor[1], ",", $valor[2], ");'>
            <a href='ejercicio01_2.php?fondo=" . $valor[0] . "," . $valor[1] . "," . $valor[2] . "'>
            <div style='width:20px; height:20px;'></div>
            </a></td>";
        $contador++;
        // cada 5 colores (múltiplo de 5) salta de línea
        if ($contador % 5 == 0) {
            echo "</tr><tr>";
        }
    }
    echo "</tr></table>";
    ?>
    <form action="ejercicio01_index.php" method="POST">
        <input type="submit" name="volver" value="Añadir más colores">
    </form>
    <form action="ejercicio01_index.php" method="POST">
        <input type="submit" name="reset" value="Crear nueva paleta">
    </form>

</body>

</html>